//	Use the rquesire directive to load the express module/package
//	It will allows us to access methods and functions that will help us create easily our application or server
//	A "module" is a software component or part of a program that contains one or more routines.
const express = require("express");

//Create an application using express
//Creates an express application and sstores this in a constant variable called app.
const app = express();


//For our application server to run, we need a port to listen
const port = 4000;

//	Middlewares (THIS IS IMPORTANT TO SETUP!)
//		> is a software that provides common services and capabilities to application outside of waht offered by the operating system
//		> allow our application to read json data

//		a.
			app.use(express.json());


// 		b.
// 			This will allow us to read data from forms.
// 			By default, inforamtion recieved from the url can only be recieved a string or an array
//			By applying the option of "extended: true" this will allows us to recieved inforation in other data types such as an object which will use throughout our application.
			app.use(express.urlencoded({extended: true}));

/*	Section I: Routes
		> Express has methods corresponong to each http method
		> this routes expects to recieved a GET request at the base URI "/"
		> 
*/
	app.get("/", (request, response) => {
		// Once the route was accessed it will send a string response containing "Hello World!"
		// Compared to the previous s32 session, .end() method was used in JS module's method
		// .send() method uses the expressJS module's method instead to send a response back the client
		response.send('Hello Batch 288!')
	});


	// GET - this route expects to recieved a GET request at the URI "/hello"
	app.get("/hello", (request, response) => {
		response.send("Hello from the /hello endpoint");
	});

	// POST - This route expects to recieved a POST request at the URI "/hello"
	app.post("/hello", (request, response) => {

		
		// console.log(response);

		// request.body contains the contents/data of the request body
		// all the properties defined in our POSTMAN request will be accessible here as properties with the same name.
		console.log(request.body);

		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
	});


	// An array will store user objects when the "/signup" route is accessed
	// this will serve as our mock database
	let users = []

	app.post("/signup", (request, response) => {
		if(request.body.username !== "" && request.body.password !== ""){
			users.push(request.body);
			console.log(request.body)
			response.send(`User ${request.body.username} successfully registered!`);
		}else{
			response.send('Please enter your username and password again!');
		}
	});

	// This rout expects to recieve a PUT request at the URI "/chage/password"
	// This will update the password of the user that maches the information provided to POSTMAN
	app.put("/change-password", (request, response) => {
		// 
		let message;
		for(let index=0; index < users.length; index++){
			console.log(users[index].username)
			console.log(request.body.username)
			// if the provided username in the client/postman and the username of the current object in the loop is the same.
			if(request.body.username == users[index].username){

				users[index].password = request.body.password;
				message = `User ${request.body.username} ${request.body.password}'s password has been updated!`;

				break;
			}else{
				message = 'User does not exist!';
			}
		}
		response.send(message);
	})


// 	ACTIVITY:
//	1.
	app.get("/home", (request, response) => {
		response.send("Welcome to the home page");
	});

//	2.
	app.get("/users", (request, response) => {
		response.send(users);
	});

//	3.
	app.delete("/delete-user", (request, response) => {
		let message;

		if(users.length != 0){

			for(let index=0; index < users.length; index++){
				if(request.body.username == users[index].username){
					users.splice(index, 1)
					
					message = `User ${request.body.username} has been deleted.`

					break;
				}
			}

			if (message == undefined) {
				message = "User does not exist."
			}
		}else{
			message = "No users found."
		}

		response.send(message);

	});



//	Tells our server to listen to the port
//	If the port is accessed, we can run the server
//	retrun a message confirming that the server is running in the terminal

if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app;