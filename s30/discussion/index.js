// console.log("Hello");

/*	Section I: MongoDB Aggregation

		- use to generate anipulated data and perform operations to create filtered results that helps analyzing data.

		- compare to doing CRUD Operations on our data from previous seesons, aggregation gives us access to manipulate, filter and compute for sesults providing us with information to make necessary development decisions without having to create a frontend application.

*/

/*		A. Using aggregate() method

			- 
			- Link 1: https://www.mongodb.com/docs/manual/reference/operator/aggregation/
			- Link 2: https://www.mongodb.com/community/forums/t/performing-aggregate-on-two-collections/168861/2

			Syntax:

*/
/* 			2. $match operator
*/
				db.fruits.aggregate([
					{$match: {onSale: true}}
				])

/* 			3. $group operator with $sum operator
 				- groups the documents in terms of the property declared in the "_id" property
 				- the next property can have different naming. In this example we use "total".
*/
				db.fruits.aggregate([
					{$group: {
						id:"$supplier_id",
						total:{$sum: "$stock"}
					}}
				])

/* 			1. $match operator and $group operator
*/
				db.fruits.aggregate([
					{$match: {onSale: true}},
					{$group: {_id:"$supplier_id", total:{$sum: "$stock"}}}
				])

/*			4. $max operator

*/
//				a. 
				db.fruits.aggregate([
					{$match:{onSale: true}},
					{$group:{
						_id:"$supplier_id",
						max: {$max: "$stock"},
						sum: {$sum: "$stock"}
					}}
				])

//				b. using different fieled in "_id" property
				db.fruits.aggregate([
					{$match:{onSale: true}},
					{$group:{
						_id:"$color",
						max: {$max: "$stock"},
						sum: {$sum: "$stock"}
					}}
				])

/*		B. Field Projection with Aggregation
			- The $project operator can be used

			- $project operator, usually used to removing the "_id" property from the query output
*/
				db.fruits.aggregate([
					{$match:{onSale: true}},
					{$group:{
						_id:"$color",
						max: {$max: "$stock"},
						sum: {$sum: "$stock"}
					}},
					{$project: {_id:0}}
				])

/*		C. Sorting Aggreagated Result
			- $sort operator ca be use to chage the order of aggreagated results.
			- Providing 1 value will sort the aggregated results in ascending order and -1 for descending order

			Syntax:
				{$sort: {field: 1 or -1}}
*/
// 			Example:
			db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group:{
					_id: "$supplier_id",
					total: {$sum: "$stock"}
				}},
				{$sort: {total: -1}}
			])
// 			Example:
			db.fruits.aggregate([
				{$match: {onSale: true}},
				{$group:{
					_id: "$name",
					stocks: {$sum: "$stock"}
				}},
				{$sort: {_id: -1}}
			])
// 			Example:
			db.fruits.aggregate([
				{$group:{
					_id: "$onSale",
					stocks: {$sum: "$stock"}
				}},
				{$project: _id: 0},
				{$sort: {_id: -1}}
			])

/*		D. Aggregating Result based on an array field
			- $unwind operator
			- this deconstruct an array field from a collection/field with an array value to output a result for each element

			Syntax:
				{$unwind: field}				
*/
			db.fruits.aggregate([
				{$unwind: "$origin"}
			])

/*			- Display fruits documents by their origin and the kinds of fruits that are supplied
			- {$sum: 1} it will count the number of documents in the group
*/
			db.fruits.aggregate([
				{$unwind: "$origin"},
				{$group:{
					_id: "$origin",
					kinds: {$sum: 1}
				}},
				{$sort: {kinds: 1}}
			])


			db.fruits.aggregate([
				{ $unwind : "$origin" },
				{ $group : {
					_id: "$origin",
					kinds: { $sum: 1 }
				}},
				{ $sort : { kinds : 1, _id: 1}}
			])

/*		E. 
*/