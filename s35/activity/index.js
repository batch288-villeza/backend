const express = require("express");
const mongoose = require("mongoose");

const port = 3001;

const app = express();


/*	SECTION I: MongoDB Connection
		- Connect to database by passing your connection string
		- Due to the update in MongoDB drivers that allow connection, the default connection string is bveing flagged as an error.
		- By default a warning will be displayed in the terminal when the application is running.
		- By adding the {userNewUrlParser:true} it adhere to the errors.

		Syntax:
			mongoose.connect("MongoDB string", {userNewUrlParser:true});
*/

	mongoose.connect("mongodb+srv://admin:admin@batch288villeza.9gjjxwu.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});
/*
		a. mongoose connection notification
			- to check the database connection is successful.
*/
	
	let db = mongoose.connection;
	// .on() - for catching the error hust incase we had an error during the connection
	// console.error.bind() - allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "error! Can't connect to database."));

	// once() - display if the connection is successfull
	db.once("open", () => console.log("We are successfully connected to the cloud database!"));

/*	Section II: Mongosse Schemas
		- Schema's determine the structure of the document to be written in the database
		- Schema's acts as blueprint to our data
		- Schema() constructor - it is used or mongoose module to create a new scehma object
*/
	const taskSchema = new mongoose.Schema({
		// Defines the fields with the corresponding data type, field: datat-type
		name: String,

		// kapag is lang ilalagay sa field oks na walang {}. if madami ilalagay sa field just use {} like the "status" field
		status: {
			type: String,
			default: "pending"
		}
	});


/*	Section II: Models
		- it uses schema and use to create/instantiate objects that corresponds to the schema.
		- Models use schema and they act as the middleman from the server or JS COde to our database.
		- to create a model, we are going to use the model()
*/
	const Task = mongoose.model('Task', taskSchema);


	/* Middleware */
	app.use(express.json());
	app.use(express.urlencoded({extended: true}));

/*	Section III: Routes
		1. Create a POST Route to create a new Task

			a. POST Create task
				- Business Logic
					1. Add a functionality to check wether there are duplicate tasks
						- if the tasks is existing in the database, we return an error
						- if the task doesn't exist in the database, we add it in the database
					2. The task data will be coming from the request's body
*/
	app.post("/tasks", (request, response) => {
		console.log(request.body)

		// "findOne()" method is mongoose method that acts similar to "find" in MongoDB.
		// criteria
		// if the "findOne()" method finds a documnet that matches the criteria, it will return the object/document and if there's no object that mathces the criteria it will retrun an empty object or null.
		Task.findOne({name: request.body.name})
			.then(result => {

				// if(result){ - truthy condition
				if(result !== null){
					return response.send("Duplicate task found!");
				}else{					
					// create new task and save it to the database
					let newTask = new Task({
						name: request.body.name
					})

					// The save() method will store the information to the database.
					// Since the newTask was created from the Mongoose Schema and tasks model, it will be save in tasks collection.
					newTask.save()

					return response.send('New task created!')
				}
			})
	});

/*	2. GET - Get all the tasks in our collection
		1. Retrieve  all the documents.
		2. If an error is encountered, print the error.
		3. if no error found, send a success status to the client and show the documents retrieved.
*/
	app.get("/tasks", (request, response) =>{
		// find() method - is a mongoose method that is similar to MongoDB find
		Task.find({}).then(result =>{
			return response.send(result);
		}).catch(err => response.send(error));
	});


/* S35 ACVITYT */
//	Item 1:
	const userSchema = new mongoose.Schema({
		username: String,
		password: String
	});
//	Item 2:
	const User = mongoose.model('User', userSchema);

//	Item 4:
	app.post("/signup", (request, response) => {
		console.log(request.body);
		
		User.findOne({username: request.body.username})
		.then(result => {
			if(result != null){
				return response.send("Duplicate username found");
			}else{
				if(request.body.username != null && request.body.password != null){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					});
					newUser.save()

					return response.status(201).send(`New user registered: ${request.body.username}`);
				}else{
					return response.send("BOTH username and password must be provided.");
				}
			}
		}).catch(err => response.send(err));
	});


if(require.main === module){
	// .listen()
	app.listen(port, () => console.log(`Server is running at port ${port}.`));
};
module.exports = app;