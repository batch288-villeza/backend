// 4-a. require task.js module
const Task = require('../models/task.js');


// 4-b. Controllers
//	a. GET - This controller will get/retrieve the documnet from the tasks colection
module.exports.getAllTasks = (request, response) => {
	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		// this uses mutiline function
		return response.send(error)
	});
}


//	b. POST - Create a controller that will add data in our database
module.exports.addTasks = (request, response) => {
	Task.findOne({name: request.body.name})
	.then(result => {
		if(result != null){
			return response.send("Duplicate Task");
		}else{
			let newTask = new Task({
				"name": request.body.name
			})
			newTask.save()
			return response.send("New task created!");
		}
	})
	// this use a single line function
	.catch(error => response.send(error));
}

// 	c. DELETE - delete specific task/document
module.exports.deleteTask = (request, response) =>{
	
	console.log(request.params.id);

	// strore in a variable to be more dynamic
	let taskToBeDeleted = request.params.id;

	// in mongoose, we have the findByIdandRemove() method that will look for a documnet with the same id provided from the URL and remove/delete the documnet from MongoDB.
	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The documnet that the _id of ${taskToBeDeleted} has been deleted!`);
	})
	.catch(error => response.send(error));
};


// S36: Activity

// GET - get specific task
module.exports.getSpecificTask = (request, response) => {
	
	let taskToBeSelect = request.params.id

	// console.log(taskToBeSelect);

	Task.find({_id: taskToBeSelect})
	.then(result => {
		return response.send(result);
	})
	.catch(err => response.send(err));
};

// PUT - changing status task
module.exports.changeStatus = (request, response) =>{
	
	const taskIdToBeChangeStatus = request.params.id

	const filter = {_id: taskIdToBeChangeStatus};
	const update = {status: "complete"}
	
	// console.log(taskIdToBeChangeStatus);
	// console.log(filter);
	// console.log(update);

	Task.findOneAndUpdate(filter, update, {new: true})
	.then(result => {
		return response.send(result);
	})
	.catch(err => response.send(err));
};
