// 5-a. express directive
const express = require("express");

// 5-b. requires taskControllers.js module
const taskControllers = require("../controllers/taskControllers.js");

// 5-c. create a Router() method. This will contain all endpoints of our application
const router = express.Router();

// 5-d. 
router.get("/", taskControllers.getAllTasks);
router.post("/addTask", taskControllers.addTasks);

/*	Parametarizer
		- We will create a route using a Delete Method at the URL "/tasks/:id"
		- the colon here is an identifier that helps to create a dynamic route which allows us to supply information
		- the colon here server as the target of what you want to delete..
	*/
router.delete("/:id", taskControllers.deleteTask);


// S36: Activity

// GET - get specific task route
router.get("/:id", taskControllers.getSpecificTask);

// PUT - changing status task route
router.put("/:id/complete", taskControllers.changeStatus);


// 5-e. module.export - is a way for NodeJS to treat this value as a package that can be used by other files
module.exports = router;