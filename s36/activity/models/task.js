// 3-a. mongoose directive
const mongoose = require("mongoose");

// 3-b. mogoose.Schema() constructor
const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		default: "pending"
	}
});

// 3-c. mongoose model. Typically use in mongoose function
const Task = mongoose.model('Task', taskSchema);

// 3-d. module.export - is a way for NodeJS to treat this value as a package that can be used by other files
module.exports = Task;