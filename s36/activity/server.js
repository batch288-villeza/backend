// 1-a. express directive
const express = require('express');
// 2-a. mongoose directive
const mongoose = require("mongoose");

// 6-a. requires the modules from routers/taskRoutes.js
const taskRoutes = require("./routers/taskRoutes.js");

// 1-b.
const app = express();
const port = 4000;


// 2-b. Setup Mongoose databse strring connection
mongoose.connect("mongodb+srv://admin:admin@batch288villeza.9gjjxwu.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});
// 2-c. Mongoose Database connection Variable
let dbConnection = mongoose.connection;
	dbConnection.on("error", console.error.bind(console, "Error! Can't connect to database."));
	dbConnection.once("open", () => console.log("Successfully connected to the cloud database!"));

// 1-c. Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 6-b. This route/middleware is responsible
app.use("/tasks", taskRoutes);


if(require.main === module){
	// 1-d. listen() method
	app.listen(port, () => console.log(`Server running at port ${port}`));
}
module.exports = app