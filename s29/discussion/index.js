// console.log("Hello");

/*
	[SECTION 1:] Advance Queries

*/

// 	a. Query Embeded Document / Object
db.users.find(
	{
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}
	}
)

// 		1. Dot Notaion
db.users.find(
	{
		"contact.email": "stephenhawking@gmail.com"
	}
)

// 		2. Querying Array with exact element
db.users.find(
	{
		courses: ["CSS", "Javascript", "Python"]
	}
)

db.users.find(
	{
		courses: {
			$all: ["React", "Laravel", "Sass"]
			// This works like an AND operator
		}
	}
)

/*	[Section 2]: Comparison Operators
		I. $gt/$gte operators
			- allows us to find documents that have field number values greater that or greater that equal to a specified value.
			- Note taht this operator will only work if the data type of the field is number or integer.

			Syntax:
				db.collectionName.find(
					{
						field: {
							$gt: value
						}
					}
				)

				db.collectionName.find(
					{
						field: {
							$gte: value
						}
					}
				)
*/
// using $gt
db.users.find({age: {$gt: 76}});

// using $gte
db.users.find({age: {$gte: 76}});

/*
		II. $lt/$lte operators
			- allows us to find documents that have field number values less than or less than equal to a specified value.
			- Note taht this operator will only work if the data type of the field is number or integer.

			Syntax:
				db.collectionName.find({
					field: {
						$lt: value
					}
				})

				db.collectionName.find({
					field: {$lte: value}
				})
*/
// using $lt
db.users.find({age: {$lt: 76}});

// using $lte
db.users.find({age: {$lte: 76}});

/*
		III. $ne operator
			- allows us to find documents that have field number values not equal to specified value
			
			Synatx:
				db.collectionName.find({
					field: {$ne: 76}
				})
*/
// using $lte
db.users.find({age: {$ne: 76}});

/*		IV. $in operator
			- allow us to find documents with specific match criteria one field using different values.

			Syntax:
				db.collectionName.find({
					field: {$in: value}
				})

*/

// using $in
db.users.find({
	lastName: {$in: ["Hawking", "Doe"]}
});

// using $in in an object
db.users.find({
	"contact.phone": {$in: ["87654321"]}
});

// using $in in an array
db.users.find({
	courses: {$in: ["React"]}
});

db.users.find({
	courses: {$in: ["React", "Laravel"]}
});


/*	[Section 3]: Logical Query Operators
		I. $or operator
			- allows us to find documents that match a single criteria from multiple provied search criteria

			Syntax:
				db.collectionName.find({
					$or: [
						{fieldA: valueA},
						{fieldB: valueB},
						...
					]
				})
*/

// using $or oprator
db.users.find({
	$or: [
		{firstName: "Neil"},
		{age: 76}
	]
});

// using $or oprator and add multiple operators
db.users.find({
	$or:
	[
		{firstName: "Neil"},
		{age: {$gt:25}},
		{"contact.phone": {$in: ["87654321"]}},
		{courses: {$in: ["HTML"]}}
	]
});

/*		I. $and operator
			- allows us to find documents that match all criteria is a single field

			Syntax:
				db.collectionName.find({
					$and: [
						{fieldA: valueA},
						{fieldB: valueB},
						...
					]
				})
*/

// using $and oprator
db.users.find({
	$and:
	[
		{age: {$ne: 82}},
		{age: {$ne: 76}}
	]
})



/*
	[Mini-Activity]
		- you are going to query all the documents from the users collection, that follow these criteria:
			1. infromation of the documents that age is older that 30 and that is enrolled in CSS or HTML
*/
db.users.find({
	$and:
	[
		{age: {$gt: 30}},
		{courses: {$in: ["HTML", "CSS"]}}
	]
})


/*	[Secion 4]: Field Projection
		- retrieving documents are common operations that we do and by default MongoDB queries retrun the whole document as a response.

		a. Inclusion
			- allows us to include or add specific fields only when retrieving documents:

			Syntax:
				db.collectionName.find(
					{criteria},
					{field: 1}
				);
*/
db.users.find(
	{firstName: "Jane"},
	{firstName:1, lastName:1, 'contact.phone':1, courses:1}
);

/*		b. Exclusion
			- allows us to remove specific fields only when retrieving documents:

			Syntax:
				db.collectionName.find(
					{criteria},
					{field: 0}
				);
*/

db.users.find(
	{$or:[{firstName: "Jane"}, {age:{$gte: 30}}]},
	{department:0, contact:0}
);

// Using $slice
db.users.find(
	{$or:[{firstName: "Jane"}, {age:{$gte: 30}}]},
	{department:0, contact:0, courses: {$slice: 1}}
);

/*	[Evaluation Query Operator]
		I. $regex operator
			- allows us to find documents that match a specific string pattern using regular expressions.

			Syntax:
				db.collectionName.find(
					{
						field: {
							$regex: 'patter',
							$options: 'optionValue'
						}
					}
				)
*/

// Case sensitive
db.users.find({
	firstName: {$regex: 'N'}
})

// Case insensitive
db.users.find({
	firstName: {$regex: 'N', $options: 'i'}
})