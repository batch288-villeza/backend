// console.log("Hello");

/*
	[SECTION I]: Functions Parameters and Arguments
		- Function Parameters
		- Function Arguments
*/

	// Example 1:
	// 	a. Create a normal function without any parameter
			function printInput(){
				let nickname = "Chris";

				console.log("Hi, " + nickname);
			};

			printInput();

	// 	b. Using a dunction parameter
	// 		1. For other cases, functions can also process data directly passed into it instead of relying only on Global Variables.
	// 		2. Consider this function:
			function printName(name){
				let nickname = "Chris";

				console.log("My name is " + name);
			};
			printName("Juana");
			printName("Chris");

	// 		3. variables can also be passed as an argument
			let sampleVariable = "Curry";
			let sampleArray = ["Davis", "Green", "Jokic", "Tatum"];

			printName(sampleVariable);
			printName(sampleArray);

	// Example 2:
	// 		- One example of using the Parameter and Argument.
	// 			a. functionaName(number);

			function checkDivisibilityBy8(num){
				let remainder = num % 8;
				console.log("The remainder of", num, "divided by 8 is:", remainder);

				let isDivisibleBy8 = remainder === 0;
				console.log("Is divisible by 8?");
				console.log(isDivisibleBy8);
			}

			checkDivisibilityBy8(64);

/*
	[SECTION II]: Functions as Arguments
*/
	// a. Function parameters can also accept other functions as argument
			// regular function
			function argumentFunc(){
				console.log("This function was passed as an argument befor the message was printed!");
			};

			// function that will pass a function as an argument
			function invokeFunc(func){
				func;
			}

			// call the invokefunc()
			invokeFunc(argumentFunc());
	
	// b. function as an argument with return keyword
			function returnFunc(){
				let name = "Chris";
				return name;
			};

			function invokeFunction(func) {
				console.log(func);
			}

			invokeFunction(returnFunc());

/*
	[SECTION III]: Multiple Arguments
		- multiple arguments will correspond to the number of parameters declared in a function in secceeding order.
*/

	// Example 1:
			function createFullName(fname, mname, lname){
				console.log(fname, mname, lname);
			}
			createFullName("Juan", "Dela", "Cruz");

	//  - In JavaScript, providing more or less arguments that the expected will not return an eror.
	//  - Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter
			createFullName("Gemar", "Cruz");

	// 	- In other programming laguages, this will return an error stating that "the number" of arguments do not match the number of parameters.
			createFullName("Lito", "Masbate", "Galan", "Jr.");

	// Example 2:
	// 		- using variables as Multiple Arguments

			let fName = "John"
			let mName = "Doe"
			let lName = "Smith"

			createFullName(fName, mName, lName);

/*
	[SECTION IV]: JavaScript alert() and prompt()
		a. alert()
			- allows us to show a small window at the top of our browser page to show information to our users. As opposed to console.log which only shows the message on the console.

			SYNTAX:
				alert("messageInString");
*/

			// alert("Hello World"); // This will run immediately when the page loads
			
			// alert inside a function
			function showSampleAlert(){
				alert("Hello World");
			}
			// showSampleAlert();

			console.log("I will only log in the console when the alert is dismissed!");

	// 	b. prompt()
	//		- it allows us to show a small window at the top of the browser to gather user input
	// 		SYNTAX: prompt("dialofInString");

	// Example 1:
			let samplePromt = prompt("Enter your name: ");
			console.log(typeof samplePromt);
			console.log("Hello,", samplePromt);

	// Example 2:
			let age = prompt("Enter your age:");
			console.log(typeof age);
			console.log(age);

	// Example 3:
	// 		- returns an empty string whne there is no input or null if the user cancels the prompt
			let sampleNullPrompt = prompt("Don't enter anything.");
			console.log(sampleNullPrompt);

	// Example 4:
			function printWelcomeMessage(){
				let firstName = prompt("Enter your First name:");
				let lastName = prompt("Enter your Last name:");

				console.log("Hello, " + firstName + " " + lastName + "!");
				console.log("Welcome to my page!");
			}
			printWelcomeMessage();