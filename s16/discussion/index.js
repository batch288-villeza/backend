// To check script is properly linked to HTML file
// console.log("Hello")

/*
	[SECTION I]: Arithmethic Operators
*/

	let x = 10;
	let y = 4;

	// Addition Operators (+)
	let sum = x + y;
	console.log("Sum Result of addition operator: " + sum);

	// Subtraction Operators (-)
	let difference = x - y;
	console.log("Difference Result of subtraction operator: " + difference);

	// Multiplication Operator (*)
	let product = x * y;
	console.log("Product Result of multiplication operator: " + product);

	// Division Operator (/)
	let qoutient = x / y;
	console.log("Qoutient Result of division operator: " + qoutient);

	// Modulo Operator (%)
	let remainder = x % y;
	console.log("Remainder Result of modulo operator: " + remainder);



// [SECTION II]: Assignment Operators

/*
	a. Assignment Operators
		- assigns/reassign a value to the variable
*/
	// Exxample 1:
	let assignmentNumber = 8;
	console.log(assignmentNumber);

/*
	b. Addition Assignment Operators
		- adds value to the right operand to a variable and assigns the result to the variable
*/	
	// Exxample 1:
	assignmentNumber += 2;
	console.log("Addition Assignment Operator Result: ", + assignmentNumber);

	// Exxample 2:
	assignmentNumber += 3;
	console.log("Subtraction Assignment Operator Result: ", + assignmentNumber);

/*
	c. Subtraction Assignment Operators
		- 
*/	
	// Exxample 1:
	assignmentNumber -= 2;
	console.log("Subtraction Assignment Operator Result: ", + assignmentNumber);

/*
	d. Multiplication Assignment Operators
		- 
*/	
	// Exxample 1:
	assignmentNumber *= 3;
	console.log("Multiplication Assignment Operator Result: ", + assignmentNumber);

/*
	e. Division Assignment Operators
		- 
*/	
	// Exxample 1:
	assignmentNumber /= 11;
	console.log("Division Assignment Operator Result: ", + assignmentNumber);

/*
	f. Multiple Operators and Parentheses
*/
	// 1. MDAS Rules: Multiplication or Division first then Addition or Subtraction, From Left to right.
	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log(mdas);
	/*	
		HOW IT Works:
		= 1 + 2 - 3 * 4 / 5
		= 1 + 2 - 12 / 5
		= 1 + 2 - 2.4
		= 1 + 2 - 2.4
		= 3 - 2.4
		= 0.6
	*/

	// 2. PEMDAS Rules: 
	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log(pemdas);
	/*
		= 1 + (2 - 3) * (4 / 5)
		= 1 + (-1) * 0.8
		= 1 + (-0.8)
		= 0.2
	*/

/*
	[SECTION III]: Increment and Decrement
		- it add or subtract values by 1 and reassigns the value of the variable where the increment and decrement applied to.
*/
	// Increment
	// 	1. pre-increment
	let i = 1;
	let increment = ++i;
	console.log("Pre-Increment Result: " + increment);
	console.log("Pre-increment Value: " + i);

	// 	2. post-increment
	increment = i++;
	console.log("post-Increment Result: " + increment);
	console.log("post-increment Value: " + i);

	// Decrement
	// 	1. pre-Decrement
	let e = 1;
	let decrement = --e;
	console.log("Pre-Decrement Result: " + decrement);
	console.log("Pre-Decrement Value: " + e);

	// 	2. post-Decrement
	decrement = e--;
	console.log("post-Decrement Result: " + decrement);
	console.log("post-Decrement Value: " + e);


/*
	[SECTION IV]: Type Coercion
		- is the automatic or implicit converion of values from one data type to another.
		- this happens when operations are performed on different data types that would normally possible and yield irregular results.
*/

	let numA = '20';
	let = numB = 23;

	let coercion = numA + numB;
	// if you are going to add string and number, it will concatenate its value.
	console.log(coercion);
	console.log(typeof coercion);


	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;

	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	// Implicint conversion
	// The boolean "true" is also associated with the value of 1
	let numE = true + 1;
	console.log(numE);

	// the boolean "false" is also associated with the value of 0
	let numF = false + 1;
	console.log(numF);

/*
	[SECTION V]: Comparision Operators
		- 
*/

	let juan = 'juan';

/*
	a. Equality Operator (==)
		- checks wether the operand are equal/have the same content/value.
		- returns a boolean (true or false) value.
		- attempts to CONVERT AND COMPARE operands of different data types.
*/
	// Example 1: compare number
	console.log("1 == 1:", 1 == 1);			//true
	console.log("1 == 2:", 1 == 2);			//false
	console.log("1 == '1'", 1 == '1')		//true
	console.log("0 == false:", 0 == false); //true


	// Example 2: Compare two srtings
	console.log("'jaun' == 'juan':", 'jaun' == 'juan');	//true
	console.log("'JUAN' == 'juan':", 'JUAN' == 'juan');	//false (case sensitive)

	console.log("juan == 'juan':", juan == 'juan');	//true

/*
	b. Inequality Operator (!=)
		- checks whether the operands are not equal/have different content/values.
		- returns boolean value
		- attempts to CONVERT AND COMPARE operands of different data types.
*/
	// Example 1:
	console.log("1 != 1:", 1 != 1); 		//false
	console.log("1 != 2:", 1 != 2);			//true
	console.log("1 != '1'", 1 != '1')		//false
	console.log("0 != false:", 0 != false); //false


	// Example 2: Compare two srtings
	console.log("'jaun' != 'juan':", 'jaun' != 'juan');	//false
	console.log("'JUAN' != 'juan':", 'JUAN' != 'juan');	//true (case sensitive)

	console.log("juan != 'juan':", juan != 'juan');	//false


/*
	c. Strict Equality Operator (===)
		- checks whether the operands are equal/have the same content.
		-also compares the data types of the two values.
*/

	// Example 1:
	console.log("1 === 1:", 1 === 1); //true
	console.log("1 === 2:", 1 === 2); //false
	console.log("1 === '1'", 1 === '1'); //false
	console.log("0 === false:", 0 === false); //false


	// Example 2: Compare two srtings
	console.log("'jaun' === 'juan':", 'juan' === 'juan'); //true
	console.log("'JUAN' === 'juan':", 'JUAN' === 'juan'); //flase (case sensitive)

	console.log("juan === 'juan':", juan === 'juan'); //true

/*
	d. Strict Inequality Operator (!==)
		- checks whether the operands are not equal/have different content.
		- also compares the data types of the two values.
*/

	// Example 1:
	console.log("1 !== 1:", 1 !== 1); //false
	console.log("1 !== 2:", 1 !== 2); //ture
	console.log("1 !== '1'", 1 !== '1'); //true
	console.log("0 !== false:", 0 !== false); //true


	// Example 2: Compare two srtings
	console.log("'jaun' !== 'juan':", 'juan' !== 'juan'); //false
	console.log("'JUAN' !== 'juan':", 'JUAN' !== 'juan'); //ture (case sensitive)

	console.log("juan !== 'juan':", juan !== 'juan'); //false

/*
	[SECTION VI]: Relational Operators
		- checks wether 1 value is greater than or less than to the other values.
*/
	let a = 50;
	let b = 65;

	// a. Greater Than Operator (>)
	let isGreaterThan = a > b;	
	console.log("a > b:", isGreaterThan);	//false

	// b. Less Than Operator (<)
	let isLessThan = a < b;
	console.log("a < b:", isLessThan);	//false

	// c. Greater Than or Equal Operator (>=)
	let isGTorEqual = a >= b;
	console.log("a >= b:", a >= b); //false

	// d. Less Than or Equal Operator Operator (<=)
	let isLTorEqual = a <= b;
	console.log("a <= b:", isLTorEqual); //true

	let numStr = "30";
	console.log("a > numStr:", a > numStr);	//false
	console.log("b <= numStr:", b <= numStr);	//true

	let strNum = 'twenty';
	console.log("b >= strNum:", b >= strNum);	//false

	// since the string is not numeric, the string was converted to a number and it resulted to Nan (Not A Number)

/*
	[SECTION VII]: Logical Operators
		-
*/
	let isLegalAge = true;
	let isRegistered = false;

	// a. Logical AND Operator (&& - Double Ampersand) - Returns "true" if all operands are true.
	// isRegistered = true; //uncommment to check a true result.
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND Operator:", allRequirementsMet);

	// b. Logical OR Operator (|| - Double Pipe) - returns true if one of the operands are true
	// isLegalAge = false; //uncomment to check false rsult
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR Operator:", someRequirementsMet);

	// c. Logical NOT Operator (! - Exclamation Point)
	// isRegistered = true //uncomment to check false value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator:", someRequirementsNotMet);