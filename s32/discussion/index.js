const http = require('http');

const port = 4000;

const server = http.createServer((request, response)=>{

    // inside if statement declares the HTTP request method

    // Using GET Request
    if(request.url == '/items' && request.method == 'GET'){
        //writeHead(status_code, {'Content-Type': type})
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Data retrieved from database.');
    };
    if(request.url == '/' && request.method == 'GET'){
        //writeHead(status_code, {'Content-Type': type})
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end(`localhost:${port}`);
    };

    // Using POST Request
    if(request.url == '/items' && request.method == 'POST'){
        //writeHead(status_code, {'Content-Type': type})
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Data to be sent to database.');
    };

    
});

server.listen(port);

console.log(`Server is now accessible from localhost:${port}`);


/*  Using postman
        * http://localhost:4000/items

    end port process:
        * npx kill-port "port_number"
            > npx kill-port 4000
*/