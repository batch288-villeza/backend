let http = require("http");

// mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
]


const port = 3000;

const server = http.createServer((request, response)=>{

    // inside if statement declares the HTTP request method

    // Using GET Request
    if(request.url == '/users' && request.method == 'GET'){
        // writeHead(status_code, {'Content-Type': type})
        // 'application/json' Content-Type , sets response output to JSON data type
        response.writeHead(200, {'Content-Type': 'application/json'});
        
        // response.write() - is method in Node.js that used to write data to the response body in a HTTP Server
        // JSON>stringyfy() - is a method that converts the string input to JSON
        response.write(JSON.stringify(directory));

        // response endpoint
        response.end();
    };

    // Using POST Method
    if (request.url == "/addUser" && request.method == "POST") {
        
        // decalred and initialized a "requestBody" variable to an empty string
        let requestBody = "";

        // request.ont() - event listener in Node.js that is used to handle incoming data in HTTP server
        // data - is recieved from the client and is processed in the "data" stream.
        request.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

        // response end step - only runs after the request has completely been sent
		request.on('end', function() {

            // Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);
            
            // Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			}

            // Add the new user into the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();

        });
    }
});

server.listen(port);

console.log(`Server is now accessible from localhost:${port}`);

/*  Using postman
        * http://localhost:3000/items

    end port process:
        * npx kill-port "port_number"
            > npx kill-port 3000
*/