// declare a directive express to use the Router() method
const express = require("express");
const router = express.Router();

const usersControllers = require("../controllers/usersControllers.js");
const auth = require('../auth.js')

// Routes

router.post("/registration", usersControllers.registerUser);

// router.get("/login", usersControllers.loginUser);
router.post("/login", usersControllers.loginUser);

// will go first to auth.verify before proceeding to 
router.get("/details", auth.verify, usersControllers.getProfile);

// route for course enrollement
router.post("/enroll", auth.verify, usersControllers.enrollCourse);

//
router.get("/userdetails", usersControllers.retrieveUserDetails);

// check email
router.post("/verifyEmail", usersControllers.checkEmail);

// Token Verification: this is only for checking
// router.get("/verify", auth.verify);

module.exports = router;

