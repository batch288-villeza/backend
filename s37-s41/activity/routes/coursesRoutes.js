const express = require('express');

const coursesControllers = require('../controllers/coursesControllers.js');

const router = express.Router();

// Require auth.js
const auth = require('../auth.js')

// Without parameters

// This route is responsible for adding course in our db
router.post("/addCourse", auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses
router.get("/", auth.verify, coursesControllers.getAllCourses);

// Route for retrieving all active courses
router.get("/activeCourses", coursesControllers.getActiveCourses);


// 
router.get("/inactvieCourse", auth.verify, coursesControllers.getInactiveCoureses)


// With parameters

//
router.get("/:courseId", coursesControllers.getCourse)


// router for updating course
router.patch("/:courseId", auth.verify, coursesControllers.updateCourse)


// router for archive course
router.patch("/:courseId/archive", auth.verify, coursesControllers.archiveCourse)



module.exports = router;