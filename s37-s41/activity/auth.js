const jwt = require('jsonwebtoken');


// User defined string that will be used to create our JSON Web Tokeb.
// Used in the algorithm for encrypting out data which makes it difficult to decode the information without the defined keyword.
const secret = "courseBookingAPI";


/*	Section: JSON Web Token (JWT)
		- is a way of securely passing information from the server to the frontend or to the parts of server.
		- Information is kept secure through the use of the secret code.

	A. Function for token:
		Analogy:
			- Pack the gift and provide a lock with the secret code as the key.

		The argument that will be pass in the parameter will be the document or bject that contains the info of the user.
			{
				_id : OnjectId,
				firstName: ,
				lastName:,
				other properties
			}

	B. Generate Token (Authentication):
		> arrow function parameter
			- 
		>
*/
module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		isAdmin: user.isAdmin,
		email: user.email
	}

	/*	Generate a JSON Web Token using JWT's ".sign()" method
		generate the token using the form data and the secret code with no additional options provided.
	*/
	return jwt.sign(data, secret, {});
}


/***********************************************************************************************************************************************************/
/*	Token Verification

		Analogy:
			Recieve gift, open and lock to verify if the sender is legitimate and the gift was not tampered
*/

module.exports.verify = (request, response, next) =>{

	let token = request.headers.authorization;
	// console.log(token); // the authentication code as token

	// ifelse statememt: create a condition the checks if there is a token provided or not
	if(token !== undefined){
		
		/*	> By default, the returned token contains the: "Bearer<space>" + "authentication token"
			> This will cause an error since the returned token has an additional characters to it.
			> to fixed this we need to remove the unwanted chareacters and only return the authentication token by using the ".slice()" method.
			
			> Synatx:
				- token.slice(startIndex, endingIndex)
				- startIndex
					- Required. The start position. (First character is 0).
				-endingIndex
					- Optional. The end position (up to, but not including). Default is string length.
		*/
		token = token.slice(7, token.lenght)

		
		/*	Validate the token by sing the ".verify()" method in decrypting the token using the secret code.

			Syntax:
				jsonwebtoken.verify(token, secret, function)
					> token
						- is the token created from the login authentication

					> secret
						- is the secret word use in creating the authentication. This is usually save in "Environment Variable"

					> function (arrow function is mostly used for symplicity):
						- this usually captures the error if the token authenticaton was failed

						Syntax:
							- (parameter1, parameter2) => {
								if(parameter1){
									return response.send(statement);
								}else{
									return response.send(statement);
								}
							}

						- parameter1
							- this is used for the erro
						- parameter2
							- this is used to capture the data if there is error
						- if else statememnt
							- in if condition, parameter1 is used to display if there are error in the authentication
							- otherwise, the else condition will disply when authentication was successful.
						
		*/
		return jwt.verify(token, secret, (error, data) => {
			if (error) {
				return response.send(false);
			}else{

				// parameter next will allows us to proceed to the next function.
				// in this case, the next function is

				// console.log("Token has verified"); // just for checking
				next();
			}
		})
	}else{
		return response.send(false)
	}
}


/* Decode encrypted token

*/
module.exports.decode = (token) =>{
	token = token.slice(7, token.length);

/*	The decode() method is used to obtain the information from JWT
		> token
			- 
		> {complete:true}
			- option allow us to return additional information from the JWT token
		> .payload
			- 
*/
	return jwt.decode(token, {complete: true}).payload;
}