// require model (Users model)

const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js")

// Require bcrypt
const bcrypt = require("bcrypt");

// Require auth.js
const auth = require('../auth.js');

// Controllers

/* I. Create a controller for sign-up
		a. Business Logic/Flow
			1. First, we have to validate whether the user is existing or not.
				- we can validate if the email is existing in the database or not.
			2. If the user email is existing, we will prompt an error telling the user that the email is taken.
			3. Otherwise, we will sign up or add the use in our database.*/

module.exports.registerUser = (request, response) => {
	let getEmail = request.body.email;

	// console.log(`${getEmail} registerUser`);

	// find method: it will retrun an array
	// findOne method: will return an single object
	Users.findOne({email: getEmail})
	.then(result => {
		// console.log(`${getEmail} then result`);
		// we need to add if statement to verify whether the email already exist in our database.
		// if(result != null){
		if(result){
			return response.send(false);
		}else{
			
			// create new object instantiated using the Users Model
			let newUser = new Users({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,

				// hashSycn() method: it hash/encrypt our password
				//	- it ha 2 arguments to be encypted, salt rounds
				// syntax: hashSycn(to be encypted, salt rounds)
				password: bcrypt.hashSync(request.body.password, 10),

				isAdmin: request.body.isAdmin,

				mobileNo: request.body.mobileNo
			})

			// save the user
			// add error handling
			newUser.save()
			.then(saved => response.send(true))
			.catch(err => response.send(false));
		}
	})
	.catch(err => response.send(false))
};


// This use the auth.js
module.exports.loginUser = (request, response) => {
	return Users.findOne({email: request.body.email})
	.then(result =>{
		if(!result){
			return response.send(false);
		}else{
			// the compareSync() method: is used to compare a non-encrypted password from the login form to the encrypted password retrieve from the findOne method. Return true or false depending on the result of the comparison.
			// Syntax: compareSync(not encrypted, encrypted)
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				// return response.send("Login successful!");
				//
				return response.send(
					{
						auth: auth.createAccessToken(result)
					}	
				)
			}else{
				return response.send(false);
			}
		}
	})
	.catch(err => response.send(false));
}

// S38 Activity 
module.exports.getProfile = (request, response) =>{

	let userId = request.body._id;
	
	// my s38 activity input
	/*	Users.findOne({id: userId})
		.then(result => {
			return response.send({
				"_id": result._id,
				"firstName": result.firstName,
				"lastName": result.lastName,
				"email": result.email,
				"password": "",
				"isAdmin": result.isAdmin,
				"mobileNo": result.mobileNo,
				"__v": result.__v
			})
		})
		.catch(err => response.send(err));
	*/

	// Intructor's Input
	
	let userData = auth.decode(request.headers.authorization)

	// console.log(userData);

	if(userData.isAdmin){
		// return??? bakit meron nyan
		return Users.findById(userId)
		.then(result => {
			/*
				> Changes the value of the user's password to an empty string when returned to the frontend
				> Not doing so will expose the user's password which will also not be needed in other parts of our application
				> Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
			*/
			
			// Returns the user information with the password as "Confidential"
			result.password = "Confidential";

			// 
			return response.send(result);
		})
		.catch(err => response.send(err));
	}else{
		return response.send("You are not an admin, you dont't have access to this route.")
	}	
}

// Controller for course enrollment
module.exports.enrollCourse = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.body.id;

	if(userData.isAdmin){
		return response.send(false)
	}else{

		// Push to users document
		let isUserUpdated = Users.findOne({_id: userData.id})
		.then(result => {

			result.enrollments.push({
				courseId: courseId
			});

			result.save()
			.then(save => true)
			.catch(err => false)
			// response.send(result)
		})
		.catch(err => false);

		// Push to course document
		let isCourseUpdated = Courses.findOne({_id: courseId})
		.then(result => {
			result.enrollees.push({
				userId: userData.id
			})

			result.save()
			.then(saved => true)
			.catch(err => false)
		})
		.catch(err => false)

		// If condition: to check whether we updated the users document and courses document
		if(isUserUpdated && isCourseUpdated){
			return response.send(true)
		}else{
			return response.send(false)
		}
	}
}

module.exports.retrieveUserDetails = (request, response) =>{
	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id: userData.id})
	.then(data => response.send(data))
	.catch(err => response.send(err));
}

module.exports.checkEmail = (request, response) =>{
	const getEmail = request.body.email;

	Users.findOne({email: getEmail})
	.then(result => {
		if(result){
			return response.send(result)
		}else{
			return response.send(false)
		}
	})
	.catch(err => response.send(false));
}