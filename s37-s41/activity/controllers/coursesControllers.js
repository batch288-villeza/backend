const Courses = require('../models/Courses.js');


// Require auth.js
const auth = require('../auth.js');


// Controllers (Controller Function)
module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		
		// create an object using the Courses Model
		let newCourse = new Courses({

			// supply all the required
			name: request.body.name,
			description: request.body.description,
			price: request.body.price,
			isActive: request.body.isActive,
			slots: request.body.slots
		})

		newCourse.save()
		.then(save => response.send(true))
		.catch(err => response.send(false));

	}else{
		return response.send(false)
	}
}


// In this controller we are going to retrieve all of the course in our database.
module.exports.getAllCourses = (request, response) => {
	
	const userData = auth.decode(request.headers.authorization)

	if(userData.isAdmin){
		Courses.find({})
		.then(result => response.send(result))
		.catch(err => response.send(false));
	}else{
		response.send(false);
	}

}

// This will get all active coureses
module.exports.getActiveCourses = (request, response) =>{
	Courses.find({isActive: true})
	.then(result => response.send(result))
	.catch(err => response.send(false))
}


// This controller will retrieve the information of a specific coures using its ID
module.exports.getCourse = (request, response) =>{

	const courseId = request.params.courseId

	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(err => response.send(false))

}

// this controller will update a course
module.exports.updateCourse = (request, response) =>{

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;

	//update description, price and name

	let updatedCourse = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	}

	if(userData.isAdmin){
		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(err => response.send(false));
	}else{
		return response.send(false);
	}
}

// S40: Activity
// Controller for Achiving a course
module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const isCourseActive = request.body.isActive;

	const courseStatus = {
		isActive: request.body.isActive
	}

	if(userData.isAdmin){
		Courses.findByIdAndUpdate(courseId, courseStatus)
		.then(result => {
			if(isCourseActive){
				response.send(true);
			}else{
				response.send(true);
			}
		})
		.catch(err => response.send(false));
	}else{
		response.send(false);
	}
}

// Controller for Inactive Courses
module.exports.getInactiveCoureses =(request, response) =>{

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		Courses.find({isActive: false})
		.then(result => response.send(result))
		.catch(err => response.send(false));
	}else{
		return response.send(false);
	}
}