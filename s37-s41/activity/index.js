const express = require("express");
const mongoose = require("mongoose");

// 1.
//		It will allow our backend application to be available to our frontend application.
//		It will also allows us to control the app's Cross Origin Resource Sharing settings.
const cors = require("cors");

// Routes
const usersRoutes = require("./routes/usersRoutes.js");
const coursesRoutes = require("./routes/coursesRoutes.js");

const port = 4001;
const app = express();

// MongoDB Connection
// Established the connection between the DB and the applicaion server.
// Database name: CourseBookingAPI
mongoose.connect("mongodb+srv://admin:admin@batch288villeza.9gjjxwu.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

let dbConn = mongoose.connection;
	dbConn.on("error", console.error.bind(console, "Error, can't connect to cloud database"));
	dbConn.once("open", () => console.log("Successfully connected to cloud database!"));


// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// 2.
// REMINDER: we are only going to use ".cors()" alone without any domain because front end is not yet available.
app.use(cors());

// route middleware
// 1. Users
app.use("/users", usersRoutes)

// 2. 
app.use("/courses", coursesRoutes)



//.listen() method
// app.listen(port, () => console.log(`Server running in port ${port}.`));
if(require.main === module){
	app.listen(process.env.PORT || 4000, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
	});
}