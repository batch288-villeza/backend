// console.log("Hello");

/*
	[SECTION 1]: Conditional Statements
		- it allows us to control the flow of our program.
		- it allows us to run a statement/instruction if a condition is met or run separate instruction if otherwise.
*/

// 		a. if, else if, else statements
/* 			1. if statement
				- the result of the expression in the if's condition must resu;t to true, else, the statement inside the {} will not run.

				SYNTAX:
					if(condition){
						statement;
					}
*/
		// Example 1:
			let numA = -1;
			if(numA<0){
				console.log("Hello");
			}
			console.log(numA<0);

		// Example 2: Let's update the variable and run if statement with the same condition
			numA = 0;
			if(numA<0){
				console.log("Hello");
			}
			console.log(numA<0); //It will not run because the expression now results to false.
		// Example 3:
			let city = "New York";
			if (city === "New York") {
				console.log("Welcome to New York City!");
			}
			console.log(city === "New York");

/*
			2. Else If Statement
				- executes the statement if the previous conditions are false and if the specified condition is true.
				- the "Else If" staement is optional and can be added to capture additional conditions to change the flow of a program.

				SYNTAX:
					if(condition){
						statement;
					}
					else if(condition){
						statement;
					}
*/
		// Example 1:
			let numH = 1;
			if (numH > 2){
				console.log("Hello");
			}
			else if(numH < 2){
				console.log("World");
			}
			// we were able to run the else if() statement after we evaluated tha the if condition was false.

		// Example 2:
			numH = 2;
			if (numH === 2){
				console.log("Hello");
			}
			else if(numH > 1){
				console.log("World");
			}
			// else if() statement will no longer run because the if statement was able to run, the evaluation of the whole statement stops there.

			city = "Tokyo";
			if (city === "New York"){
				console.log("Welcome to New York City!");
			}
			else if(city === "Manila"){
				console.log("Welcome to Manila City, Philippines!");
			}
			else if(city === "Tokyo"){
				console.log("Welcome to Tokyo, Japan!");
			}
			// Since the first condition was false the the program will now go to the next condition and soon.. until it met the condition to true and display the statement indside that condition.

/*
		c. Else Statement
			- executes a statement if all other conditions are falls.
			- this statement is also optional and can be added to capture ay other possible result to change the flow of a program.
*/
		// Example 1:
			let numB = 0;
			if(city > 0){
				console.log("Hello for numB!");
			}
			else if(numB < 0){
				console.log("World for numB!");
			}
			else{
				console.log("Again for numB!");
			}
			// Since the preceeding if and el if statement consditions failed, the else statement was run instead.

		// Example 2: try using else statement or else if statement without an if statement
			
			// else if{
			// 	console.log("I will not run without if statement. I will cause an error")
			// 	// Uncaught SyntaxError: Unexpected token 'else'
			// }			

			// else{
			// 	console.log("I will not run without if statement. I will cause an error")
			// 	// Uncaught SyntaxError: Unexpected token 'else'
			// }

/*
		d. If, Else If, Else Statements in a Function
			- most of the times we would like to use if, else if, and else statements with functions to control the flow of our program.
*/

		// Mini-Activity 1: - We are going to create a function that will tell the Typhoon Intensity by providing the wind speed.

			function determineTyphoonIntensity(windSpeed){
				if (windSpeed < 0){
					return "Invalid wind speed!";
				}else if(windSpeed >=0 && windSpeed <=38){
					return "Torpical Depression";
				}else if(windSpeed >=39 && windSpeed <=73){
					return "Tropical Strom";
				}else if(windSpeed >=74 && windSpeed <=95 ){
					return "Signal No. 1";
				}else if(windSpeed >=96 && windSpeed <=110 ){
					return "Signal No. 2";
				}else if(windSpeed >=111 && windSpeed <=129 ){
					return "Signal No. 3";
				}else if(windSpeed >=130 && windSpeed <=156 ){
					return "Signal No. 4";
				}else{
					return "Signal No. 5"
				}
			}
			console.log(determineTyphoonIntensity(-1));
			console.warn(determineTyphoonIntensity(40));
			// Different console methods: https://tecadmin.net/javascript-console-methods/
/*
	[SECTION II]: Truthy and Falsy boolean context
		- In JavaScript a truthy value is a value that is considered true when encountered in a boolean context.
		- Falsy values are excemption for Truthy

			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undifined
			7. NaN
*/
	// Example 1: Truthy

		if (true) {
			console.log("Truthy")
		}
		if (78) {
			console.log("Truthy")
		}

	// Example 2: Falsy

		if (false) {
			console.log("Falsy")
		}
		if (0) {
			console.log("Falsy")
		}

/*
	[SECTION III]: Conditional (Ternary) Operator
		- The Ternary Operator takes in three operands:
			1. Condition
			2. Expression to execute if the condition is truthhy
			3. Expression to execute if the condition is falsy
		- It can be used to an if else statement
		- Ternary Operator have an implicit return statement meaning without "return" the resulting expression can be strored in a variable.

		Syntax:
			condition ? ifTrue : ifFlase;
*/
	// a. Single Statemet Execution
	// Example 1:
		let ternaryResult = (1 < 18) ? true : false;
		console.log("Result of Ternary Operator:", ternaryResult);
	
	// Example 2:
		let exampleTernary = (0) ? "The number is not equal to 0" : "The number is equal to 0";
		console.log(exampleTernary);

	// Multipple Statement Execution using function
		function isOfLegalAge(){
			let name = "John";
			return "You are in legal age limit, " + name;
		}

		function isUnderAge(){
			let name = "John";
			return "You are under legal age limit, " + name;
		}

		// let age = prompt("What is your age?"); // Data type is string
		// let age = parseInt(prompt("What is your age?")); // Data type is converted from string to Int using parseInt function
		// console.log(age);
		// console.log(typeof age);

		// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();

		// console.log(legalAge);

		console.log("Age greater that 18:",('20' / 18))


/*
	[SECTION IV]: Switch Statement
		- Switch Statement evaluates an expression and matches an expression and matches the expression's value to a case clause. The switch will then execute the statements associated with that case, as well as statements in cases that follow tha matching case.
		- this statement is also a case sensitive one

		Syntax:
			switch(expresion/variable){
				case value:
					statement;
					break;
				default:
					statement;
					break;
			}
*/

	// Example 1:
		let day = prompt("What day of week is today?").toLowerCase(); // the .toLowerCase() function/method will change the inpput recieved from the promt into all lowercase letters.
			console.log(day);

		switch(day){
			case "monday":
				console.log("The color of the day is red!");
				break;
			case "tuesday":
				console.log("The color of the day is orange!");
				break;
			case "wednesday":
				console.log("The color of the day is yellow!");
				break;
			case "thursday":
				console.log("The color of the day is green!");
				break;
			case "friday":
				console.log("The color of the day is blue!");
				break;
			case "saturday":
				console.log("The color of the day is indigo!");
				break;
			case "sunday":
				console.log("The color of the day is violet!");
				break;
			default:
				console.log("Please input a valid day.");
				break;
		}

/*
	[SECTION V]: Tyr - Catch - Finally Statement
		- try catch statements are conmmonly used for error handling
		- there are instances when the application return an error/warning that is not necessarily an error in contect of our code.
*/
	// Example 1:
		function showIntensityAlert(windSpeed) {
			try{
				alert(determineTyphoonIntensity(windSpeed));
			}catch(error){
				// catch will only run if and only iif, there was an error in the statement inside our try block.
				console.log(typeof error)
				console.warn(error.message)
			}finally{
				// regardless if there was an error or none, this will always run.
				alert("Intensity updates will show new alert!");
			}
		}
		showIntensityAlert(56);

		// alerat("Hi"); //intended error, to show how try catch works for error handling

		console.log("Hi, Im after showIntensityAlert function!");