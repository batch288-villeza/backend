// console.log("Hello");

/*	[SECTION 1]: JSON Objects
		- JSON stands for JAvaScript Object Notaion
		- it is also used in other programming languages hence the name JavaScript Object Notation
		- Core JavSCript has a built-in JSON obects that contains method for prshing objects and converting strings into JavaScript Objects
		- JavaSCript objects should not be confused with JSON
		- JSON is used for serializing different data types into bytes
			- SEREALIZATION is the process of converting data ito series of bytes for easier transmission/transfer of information/data
			- Byte is a unit of data that is eight binary digits (1 , 0) that is used to represent a character (letter, numbers, typographic symbol).
		- File type: .json

		Syntax:
			{
				"propertyA" : "valueA",
				"propertyB" : "valueB",
				"propertyC" : "valueC",
				...
			}

		- "See object.json file"
*/

/*	[SECTION 2]: JSON Arrays
		"cities" : [
			{
				"city": "Quezon City",
				"province": "Metro Manila",
				"contry": "Philippines"
			}
		]
*/


/*
	[SECTION 3]: JSON Methods
		- JSON Objects contains methods for parsing and converting data into stringified JSON.

		a. Converting Data into Stringified JSON
			- Stringified JSON is a JavaScript Object converted into a string to be use in other fucntion of a JavaScript application.
			- They are commonly used in HTTP request information is required to be sent  and recieve in a stringified version
			- Requests are an important part of programming where an application communicates with a backend application to perform dfferent tasks such as getting/creating data in a database.
*/

let batchesArr = [
		{batchName: "Batch X"},
		{batchName: "Batch Y"}
	]
console.log(batchesArr);
console.log(typeof batchesArr)

// the stringify method is used to convert JavaScript Objects into a string.
console.log('Result from Stingify method:');

// convert objects onto stringify objects JSON.stringify(variableName);
console.log(JSON.stringify(batchesArr));
console.log(typeof JSON.stringify(batchesArr))

/* 		b. Using stringify method with variables
			Syntax:
				JSON.stringify(
					{
						"propertyA" : "valueA",
						"propertyB" : "valueB",
						"propertyC" : "valueC",
						...
					}
				)			
*/

// User Details

// let firstName = prompt('What is your first name?');
// let lastName = prompt('What is your last name?');
// let age = prompt('What is your age?');
// let address = {
// 		city: prompt('Which city do you lived in?'),
// 		country: prompt('Which country does your city belong?')
// }

// let otherData = JSON.stringify({
// 		firstName: firstName,
// 		lastName: lastName,
// 		age: age,
// 		address: address
// });

// console.log(otherData);

/*		c. Converting Stringified into JavaScript Objects
			- Objects are common data types used in applications because of the complex data structures that can be created out of them
*/
// Example 1:
let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]';
console.log(batchesJSON);

console.log('Result of parse method');
console.log(JSON.parse(batchesJSON));

// Example 2:
let exampleObject = '{"name": "Chris", "age": 16, "isTeenager": false}';
console.log(JSON.parse(exampleObject));

let exampleParse = JSON.parse(exampleObject);

console.log(typeof exampleParse.name);
console.log(typeof exampleParse.age);
console.log(typeof exampleParse.isTeenager);

let users = [
	{
      "id": 1,
      "name": "Leanne Graham",
      "username": "Bret",
      "email": "Sincere@april.biz",
      "address": {
        "street": "Kulas Light",
        "suite": "Apt. 556",
        "city": "Gwenborough",
        "zipcode": "92998-3874",
        "geo": {
          "lat": "-37.3159",
          "lng": "81.1496"
        }
      },
      "phone": "1-770-736-8031 x56442",
      "website": "hildegard.org",
      "company": {
        "name": "Romaguera-Crona",
        "catchPhrase": "Multi-layered client-server neural-net",
        "bs": "harness real-time e-markets"
      }
    }	
]

console.log(users)
console.log(JSON.parse(users))