/*  > Use "require" directive to load Node.js modules.
    > A module is a software component or part of a program that contains one or more routines.
    > The "http module" lets Node.js transfer data using the Hyper Text Transfer Protocol.
        - In that way http is a protocol that allows the fetching of sources such as HTML documents.
    
    > Clients (browser)
        - Servers (NodeJS / ExpressJS Applications) comunicate by exchanging individual messages
        - These messeges are sent by the clients, usally a web browser and called "request".
        - The messeges sent by the server as an answer are called "response".
*/

let http = require("http");

/*  > Ushing this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives responses back to the client.
    > "Port" - is a virtual point where network connections starts and end.
    > The http modules has a createServer() method that accepts a function as an argument and allows for creation of a server.
    > The arguments passed in the function are request and response objects (data types) that contains methods that allows us to recieve request from the client and send response back to it.

    > The server will be assigned port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to it eventually communicating with our serrver
*/
http.createServer(function(request, response)
    {
        /*  > Use writeHead() method to:
                - set a status code for response - a 200 means OK
                - set content-type of the response as a plain text message
        */
        response.writeHead(200, {'Content-Type': 'text/plain'});

        //send the response with content 'Hello World?!'
        response.end('Hello World?!');
    }
).listen(8000);

// When the server is running, console will print the message:
console.log('Server running at localhost:8000');


/*  Check Mode.js Connection
    1. On the terminal, go to the index.js file directory
    2. type: node index.js
        - to check if we can connect to the localhost port 4000
    3. on the browser
        - type localhost:4000
    4. ctrl + c
        - to stop the terminal

    
    nodemon package
        1. in the terminal type: npm install -g nodemon
        2. check nodemon version:
            - nodemon --verion
        3. 
*/