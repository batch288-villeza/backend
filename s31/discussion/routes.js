const http = require('http');

// Create a variable to store the "port" number.
const port = 8888;

const server = http.createServer((request, response) =>{
    if(request.url == '/greetings'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('Hello Michael Jordan!');
    }else if(request.url == '/homepage'){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.end('This is the homepage');
    }else{
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.end('Hey! Page is not available. This is using nodemon package');
    }
});

server.listen(port);

console.log(`SErver is now accessible at localhost ${port}.`);