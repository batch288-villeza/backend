// console.log("Hello");

/*
	[SECTION 1]: ARRAY
		- an array in programming is simply a list of data that shares the same data type.
		- arrays are used to store multiple related values in a single variable.
		- there are declared using the square bracket( [] ) also known as "Array Literals".

		Syntax:
			let/const arrayName = [element1, element2, ...];
*/

	// Example 1:
		let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

		let grades = [98.5, 94.3, 89.2, 90.1];

		let computerBrands = ['Acer', 'Asus', 'Lenove', 'Neo', 'Redfox', 'Toshiba'];

	// Example 2: Possible use of an array but it is not recommended
		let mixedArrays = [12, 'Asus', null, undefined, {}];

		console.log(studentNumbers);
		console.log(grades);
		console.log(computerBrands);
		console.log(mixedArrays);

	// Example 3: Alternative way ro write an array
		let myTasks = [
				'drink html',
				'eat javascript',
				'inhale css',
				'bake saas'
			]
		console.log(myTasks);

	// Example 4: Create an array with values from variable.
		let city1 = "Tokyo";
		let city2 = "Manila";
		let city3 = "Jakarta";

		let cities = [city1, city2, city3];
		console.log(cities)

/*
	[SECTION 2]: Length Property
		- the .length property allows us to get and set the total number of items in an array.
		- it can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shrten the array by simply updating the lenght of tha array
		- Delete a specific item in an array we can employe array methods (which will be shown/discussed in the next session) or an algorithm.

*/
	// Example 1:
		console.log(studentNumbers.length);
		console.log(grades.length);
		console.log(computerBrands.length);
		console.log(mixedArrays.length);
		console.log(myTasks.length);
		console.log(cities.length);

	// Example 2:Remove element inside an array
		// myTasks.length = myTasks.length - 1;
		myTasks.length -= 1;
		console.log(myTasks);

	// Example 3: removing element inside an array using decrementation
		console.log(cities);
		cities.length--;
		console.log(cities);

	// Example 4:
	// Example 5:
	// Example 6:
		let theBeatles = ['John', 'Ringo', 'Paul', 'George'];
		console.log(theBeatles);

		theBeatles.length += 1;
		console.log(theBeatles);

/*
	[SECTION 3]: Reading from Arrays
		- Accessing array elements is one of the more common tasks that we can do with an array
		- we can do this by using the array associated with it's own index/number.

		Syntax:
			arrayName[index]
*/

		console.log(grades[0]);
		console.log(computerBrands[2]);
		console.log(theBeatles[4]);


		let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
		console.log(lakersLegends);

	//	- You can actually save/store array items in another variable
		let currentLakers = lakersLegends[2]
		console.log(currentLakers);


	// 	- you can also reassign array values using items indices
		console.log('Array before reassignment:');
		console.log(lakersLegends);

	// 	- Reassign value of the specific element
		lakersLegends[1] = "Pau Gasol";
		console.log('Array after reassignment:');
		console.log(lakersLegends);
	
	// 	- Accessing the last element of an array
		let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
		console.log(bullsLegends[bullsLegends.length-1])

	// 	- Adding elements into array
		let newArr = [];
		console.log(newArr);

		// isang element lang idadagdag
		// kapag newArr.length lang yung gagamitin mag add nang new element
		// kapag newArr.lenght-1, magrereassign naman sya nang value.
		newArr[newArr.length] = "Cloud Strife";
		console.log(newArr);

		newArr[newArr.length] = "Tifa Lockhart";
		console.log(newArr);

/*
	[SECTION 4]: Looping over an array
		- you can use for loop to  iterate over all items in an array

*/		
	// Example 1:
		for(i=0; i<bullsLegends.length; i++){
			console.log(bullsLegends[i]);
		}

	// Exxample 2:
	/*	Mini-Activity:
			- Create a loop that will check whether the element inside our array is divisible by 5
			- If the number is divisible by 5, console "the number is divisible by 5"
			- If not, console "the number is not divisible by 5"
	*/
		let numArr = [5, 12, 30, 46, 40];
		let numDivBy5 = [];
		let numNotDivBy5 = [];

		for(index=0; index < numArr.length; index++){
			if(numArr[index] % 5 === 0){
				// console.log(numArr[index] + " is divisible by 5");
				console.log(`${numArr[index]} is divisible by 5`);
				numDivBy5[numDivBy5.length] = numArr[index];
			}else{
				// console.log(numArr[index] + " is not divisible by 5");
				console.log(`${numArr[index]} is not divisible by 5`);
				numNotDivBy5[numNotDivBy5.length] = numArr[index];
			}
		}
		console.log(numDivBy5);
		console.log(numNotDivBy5);

/*
	[SECTION 5]: Multidimensional Arrays
		- are useful for storing complex data struacture.
		- creating complex array structure is not always recommended.
*/
	// Example 1:
		let chessboard = [
				["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
				["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
				['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
				['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
				['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
				['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
				['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
				['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
			];


		console.log(chessboard);
		console.log(chessboard[3][4]);
		console.table(chessboard);


/*
	[SECTION ]: Getting the total value in an array
*/
		let numbers = [5, 12, 30, 46, 40];
		

	// a. Using For Loop
		let sumForLoop = 0;
		for(let i = 0; i < numbers.length; i++){
			// sumForLoop = sumForLoop + numbers[i];
			sumForLoop += numbers[i]
			console.log(sumForLoop);
		}
		console.log('For Loop:')
		console.log(`Total Sum of the [${numbers}] is ${sumForLoop}`);

	// b. Using forEach() Method
		let sumForEach = 0;
		
		numbers.forEach( num => {
					sumForEach += num;
				}
			);
		console.log('forEach() Method:')
		console.log(`Total Sum of the [${numbers}] is ${sumForEach}`);

	// c. Using reduce() Method
		let sumReduce = numbers.reduce(
			(accumulator, currentValue) => {
					return accumulator + currentValue
				},0
			);
		console.log('reduce() Method:')
		console.log(`Total Sum of the [${numbers}] is ${sumReduce}`);
