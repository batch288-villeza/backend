// Single Line Comment: ctrl + /

/*
	Multi-line comment: ctrl + shift + /
*/

// [Section] Syntax, Statements and Comments
// Statements in programming, these are the instructions that we tell the computer to perform
// JavaScript Statement usually it ends with a semicolon(;)
// Semicolon are not required in JS, but we will use it to help us prepapre for the more strict programming language like Java
// A syntax is a programming set of rules that describeds how statements muest be constraucted


// [Section] Variables
// It is usse to contained/stored data.
// Any infromation that is used by and application is stored in what we call memory.
// When we create variables, certain portion of device memory is given a "name" that we call "variables".

// Variable Declaration
// Tells our devices that a variable name is created and is ready to store data

/*
		SYNTAX:
			let/const variableName
*/

// By deafault if you declare a variable and did not initialize its value then it will be become "undefine"
let myVariable


// console.log() is useful for printing values of a variable or certain results of code into Google Chrom Console
console.log(myVariable)


/*
	Guidelines in writing varaibles:
		1. Use the 'let' keyword followed by the variable name of your choice and use the assignment operator (=) to assign a value.
		2. Variable names should start with a lowercase character, use cameCase for multiple words.
		3. Constant Variables, use 'const' keyword.
		4. Variable names, it should be descriptive related to the value stored to it.
*/

// Declaring and Initializing Variables
// Initializing Variables - the instance when a variable is given its initial or starting value.

/*
	SYNTAX:
		let varaibleNAme = value
		const variableName = value
*/

// Example:
let productName = 'desktop computer';
	console.log(productName)

let productPrice = 18999;
	console.log(productPrice)


// In the context of certain application, some variables/information are constant and should not change.
// Example: The interest rate for a loan or savings account or mortgage must not change due to real world concers.

const interest = 3.539;
	console.log(interest);

// Reassigning Variable Value - it means changing its initial or previous into another value.
/*
	SYNTAX:
		variableName = newValue
*/

productName = 'Laptop';
	console.log(productName);

/* A. Variable Declaration, Initilization and Reassigning */
// The value of variable declared using the const keyword can't be reassigned.
/*
interest = 4.489;
	console.log(interest);

// Uncaught TypeError: Assignment to constant variable
*/

// Method 1:
// Variable Declaration
let supplier;

// Variable Initialation
supplier = "John Smith Trading"
	console.log(supplier);

// Variable Reassigning
supplier = "Uy's Trading"
	console.log(supplier);

// Method 2:
// Varaiable Declaration and Initializing
let consumer = "Chris";
	console.log(supplier);

// Variable
consumer = "Topher";
	console.log(supplier);

/* A. Constant Variable Declaration, Initilization and Reassigning */

/*
// will not work
const driver;

driver = "Waylorn Jay";
*/

/*
	var vs let/const keyword
		var - is also is used in declaring variables. but var is an EcmaScript 1 version (1997)
		let/const keyword - was introduced as a new feature in ES6(2015)

	What makes var different from let/const keyword
		- There are issues associated with variables declared/created using var, regarding hoisting
		- Hoisting is JavaScript default behaviour of moving declarations to the top.
		- In terms if varibles and constants, keyword var is hoisted and let and const does not allow hoisting.

*/

// Hoisting Example
// using var keyword
a = 5;

console.log(a);

var a;

// let/const keyword
/*
b = 6;

console.log(b);

let b;

// Uncaught ReferenceError: Cannot access 'b' before initialization
*/

/*
let/const local /global scope
	- scope is essentially means whre these varaibles are available or accessible for use
	- let and const are block scope'
	- A block is a chunck of code bounded by {}. A block in a curly braces. anything within the curly braces are block
*/

// Example:

let globalVariable; //

let outerVariable = "Global Scope Variable"; //
{
	let innerVaraible = "Local Scope Variable"; //
	console.log(innerVaraible);
	console.log(outerVariable);
	console.log(globalVariable=innerVaraible);
}

// console.log(innerVaraible); //

console.log(globalVariable);

// Multiple Varaible declarations and Initialization.
// Multiple variables may be declared in one staement

// Example:
let prodCode = "DC017", prodBrand = "Dell";
	console.log(prodCode);
	console.log(prodBrand);

// Multiple output in one console.log();
	console.log(prodCode, prodBrand);

// using a variable with a reserved keywords are not allowed
// const let = "Hi Im let a reserved Keyword";

// JavaScripts Data types
/*
1. Strings
	- are series of characters that create a word, a phrase, a sentence or anything related to creating text.
	- strings in java script can be written using either a shingle qout or a double qoute
	- in other programming languages, only the double qoutes can be used for creating strings
*/

let country = 'Philippines';
let province = 'Metron Manila'

// Concatenating strings
// Multiple strings values can be combined to create a single string using the "+" symnbol

// Example:

// a. concatinating two variables
let fullAddress = province + ', ' + country;
	console.log(fullAddress);

// b. concatinating sting and a variable
let greeting = 'I live in the ' + country;
	console.log(greeting);

/*
c. escape characters
	- the escape characters (\) in strings in combination with ohter characters can produced different effects/results.
	- "\n" this creates a next line in between text
*/
let mailAddress = 'Metro Manila\nPhilippines';
	console.log(mailAddress);

let message = "John's employees went early.";
	console.log(message);

message =  'John\'s employees went early.';
	console.log(message);

/*
2. Numbers

	a. Integers/whole number
*/
let headcount = 26;
console.log(headcount);

// b. decimal number/ fraction
let grade = 98.7
console.log(grade)

// c. Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance)

// Combining strings and number
console.log("John's grade last quarter " + grade);

/*
3. Boolean
	- True or False
	- values are normally used to create values relating to the state of certain things
*/
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried:", isMarried);
console.log("isGoodConduct:", isGoodConduct);

/*
4. Arrays
	- are special kind of data that's used to store multiple related values.
	- in javascript can store different data types but is normally used to store similar data types

	a. Similar Data Types

		SYNTAX:
			let/const arrayName = [elementA, elementB, elementC, ...];
*/

// Example:
let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades);

// b. different data types (Note: this method is not recommended)
let details = ["John", "Smith", 32, true];
console.log(details)

/*
5. Objects
	- are another special kind of data types that is use to mimic real world objects/items.
	- uses a key: value pair

	SYNTAX:
		let/const objectName = {
			propertyA: valueA,
			propertyB, valueB
		};
*/

// Example:
let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ['+630917 123 4567', '8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

/*
"typeof" operator
	- is used to determine the type of data or value of a variable. It outputs a string.
*/

console.log(typeof mailAddress);
console.log(typeof headcount);
console.log(typeof isMarried);

console.log(typeof grades);

// NOTE: Array is a special type of object with methods and function to manipulate.

/*
Constant objects and arrays

	- The keyword const is a liitle misleading
	- It does not define a constant valie. It defgines a cosntant reference to a value:

	a. Becuase of this you can not:
		- Reassign a constant value.
		- Reassign a constant array.
		- Reassign a constant object.

	b. But you can:
		- Change the elements of a constant array.
		- Change the properties of constant object.
*/

const anime = ["One piece", "One Punch Man", "Attack on titan"];
console.log(anime);

//
// anime = ["One piece", "One Punch Man", "Kimetsu no yaiba"];

anime[2] = "Kimetsu no yaiba";
console.log(anime);

/*
NULL
	- It is used to itentionally express the absence of a value in a variable.
*/
let spouse = null;
// spouse = "Maria";

console.log(spouse);


/*
UNDEFIED
	- Represents the state of a variable that has been declared but without an assigned value
*/
let fullName;
fullName = "Maria";
console.log(fullName)