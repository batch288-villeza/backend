// console.log("Hello World");

/*
	[SECTION 1]: Exponent Operator
		- before the ES6 update:
			Math.pow(base, exponent);
*/
	// 8 raise to the power of 2
	const firstNum = Math.pow(8,2);
	console.log(firstNum);

	const secondNum = Math.pow(5,5);
	console.log(secondNum);

	// After ES6 update
	const thirdNum = 8 ** 2;
	console.log(thirdNum);

	const fourthNum = 5 ** 5;
	console.log(fourthNum);

/*
	[SECTION 2]: Template Literals
		- it will allow us to write strings w/o using the concatenation operation
		- Greatly helps with the code readability
*/
	let name = "John";
	let message = "Hello " + name + "! Welcome to programming!";

	console.log(message);

	// After ES6 update
		message = `Hello ${name}! Welcome to programming!`;

	// a. Multi-line using Temaple Literals: this will display same as how you created it in  your code base.
	let anotherMessage = `${name} attended a math compitition.
	He won it by soliving the problem 8**2 with the 
	solution of ${firstNum}`;

	console.log(anotherMessage);


	// - Template literals allows us to write strings with embedded JavaScript Expressions
	// -  expressions
	const interestRate = .1;
	const principal = 1000;

	console.log(`The ineterest on your savings account is: ${interestRate * principal}`);

/*
	[SECTION 3]: Array Destructuring
		- allow us to unpack elements in an array into distinct variables
		Syntax:
			-let/const [varaibleA, varaibleB, varaibleC, ...] = arrayName;
*/	
	const fullName = ["Juan", "Dela", "Cruz"];

	let firstName = fullName[0];
	let middleName = fullName[1];
	let lastName = fullName[2];

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

	// After ES6 update
	const [name1, name2, name3] = fullName;
	console.log(name1);
	console.log(name2);
	console.log(name3);

	// This result to an error because of the "const" keyword
	// name1 = "Pedro";
	// console.log(name1)
	// Uncaught TypeError: Assignment to constant variable.
 

	// 
	let gradesPerQuarter = [98,97,95,94];
	console.log(gradesPerQuarter);

	let [firtsGrading,secondGrading,thirdGrading,fourthGrading] = gradesPerQuarter
	console.log(firtsGrading);
	console.log(secondGrading);
	console.log(thirdGrading);
	console.log(fourthGrading);

/*
	[SECTION 4]: Object Destructuring
		- allow us to unpack properties of objects into distinct variables
		- shortens the syntax for accessing properties from an object.
		Syntax:
			- let/const {propertyA, propertyB, propertyC, ...} = objectName;
*/	
	// Before ES6 Update:
	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"
	}

	console.log(person);

	// const givenName = person.givenName;
	// const maidenName = person.maidenName;
	// const familyName = person.familyName;

	// console.log(givenName);
	// console.log(maidenName);
	// console.log(familyName);

	// After
	const { maidenName, givenName, familyName} = person;

	console.log(`This is the givenName ${givenName}`);
	console.log(`This is the maidenName ${maidenName}`);
	console.log(`This is the familyName ${familyName}`);

/*
	[SECTION ]: Arrow Function
		- compact alternative syntax to a traditional functions
		
		Syntax:
			const/let varaibleName = () => {
				statement/code block;
			}
*/
	// a. without parameter
		const hello = () => {
			console.log("Hello World from the arrow function");
		}
		hello();

	// b. without parameter
		const printFullName = (fName, mName, lName) => {
			console.log(`${fName} ${mName} ${lName}`);
		}
		printFullName("John", "D", "Smith");

	// c. Arrow function can also be used with loops
		const students = ["John", "Jane", "Judy"];

		students.forEach((stud) => {
			console.log(`${stud} is a student.`);
		});

/*
	[SECTION ]: Implicit Return in Arrow Function
		- 
*/
	// Example: if a function will only run 1 line or 1 statement, the arrow function will implicitly return the value.
		const add = (x,y) => x+y;
		let total = add(10,20);

		console.log(total);


/*
	[SECTION ]: Default Function Argument Value
		- provides a defualt function argument value if none is provided when the function is invoked.
*/

		// const greet = (name)=>{ //without defualt
		// const greet = (name = "user")=>{ //with defualt
		const greet = (name = "user" , age = 0)=>{ // skip an argument and display the default value
			// return `Good morning,  ${name}!`;
			return `Good morning,  ${name}! I am ${age} yearso old.`; //display default name
		}
		// console.log(greet());
		console.log(greet(undefined,25)); //display default name

		function addNumber(x = 0, y = 0){
			console.log(x);
			console.log(y);
			return x + y;
		}

		let sum = addNumber(y=1);
		console.log(sum);


/*
	[SECTION ]: Class-Based Objects Blueprints
		- allows us to create/instantiateof objects using a class as blueprint

		Syntax:
			- class className{
				constructor(objectPropertyA,objectPropertyb){
					this.objectPropertyA;
					this.objectPropertyb;
				}
			}
*/
		class Car{
			constructor(brand, name, year){
				this.brand = brand;
				this.name = name;
				this.year = year;

				// add function/method
				this.drive = () => {
					console.log('The car is moving 150 km per hour.')
				}
			}
		}

	// Instantiate an Object
		let myCar = new Car(); // without passing any parameter
		console.log(myCar);

		myCar = new Car("Ford","",2021);
		console.log(myCar);