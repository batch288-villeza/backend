// console.log("Hello");

/*
	Commands:

		1. show databases
		2. use "dbname"
		3. show collections
		4. help
*/

/*
	CRUD Operation
		- it is the heart of any backend appication
		- mastering it is essensial for any developer especially to those who want to become a backend developer
*/

/*
	[SECTION 1]: Create (Inset Document)
		a. Insert one document
			- since MongoDB deals with objects as it's stucture for documents we can easily create them by providing objects in our method/operation.

			Syntax:
				d.collectionName.insertOne({
					object
				})
*/
	// isnertOne()
	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "123456789",
			email: "jonedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	})

/*
		b. Insert Many Document
			Synatx:
				dob.collectionName.insertMany([
					{objectA},
					{objectB}
				])
*/
	// insertMany()
	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "987654321",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "123789456",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "Sass"],
			department: "none"
		}
	])

//  	c. in this collection noticed that the collection name is "userss"
	db.userss.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "123456789",
			email: "jonedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Python"],
		department: "none"
	})

/*
	[SECTION 2]: Read Operation (Find Documents)
		Syntax:
			- db.collectionName.find();
			- db.collectionName.find({field:value});
*/

// 		a. Using the find() method, will show you the list of all the documents inside the collection
	db.users.find();

// 		b. .pretty() method, allows us to be able to view the documents returned by of terminals to be in a better format.
	db.users.find().pretty()	

// 		c. this will return the documents taht will pass the criteria given in the method
	db.users.find({firstName: "Stephen"});

// 		d. Finding document using ObjectId
	db.users.find({"_id": ObjectId("646c5652da27da08b9c7c2d6")});

// 		e. multiple criteria
	db.users.find({lastName: "Armstrong", age: 82})


// 		f. duplicate
	db.users.find({firstName: "Jane"});

// 		g.
	db.users.find({'contact.phone': "123456789"});



/*
	[SECTION 3]: Updating Documents(Update)
*/
	db.users.insertOne({
		firstName: "test",
		lastName: "test",
		age: 0,
		contact: {
			phone: "00000000",
			email: "test@gmail.com"
		},
		courses: [],
		department: "none"
	})

// 	a. Updaing Single Document
	db.users.updateOne(
		{firstName: "test"},
		{
			$set:{
				firstName: "Bill",
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				courses: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "none"
			}
		}
	);
// 		1.
	db.users.updateOne(
		{firstName: "Bill"},
		{
			$set:{
				firstName: "Chris"
			}
		}
	);

// 		2.
	db.users.updateOne(
		{firstName: "Jane"},
		{
			$set:{
				lastName: "Edited"
			}
		}
	)



/*
	b. Update multiple document

	Syntax:
		db.collectionName.updateMany(
			{criteria},
			$set: {
				field: value
			}
		)

*/

db.users.updateMany(
	{department: "none"},
	{
		$set: {
			department: "HR"
		}
	}
)

/*
	c. Replace

	Syntax:
		db.collectionName.replaceOne(
			{criteria},
			{object}
		)
*/
db.users.insertOne({firstName: "test"})

db.users.replaceOne(
	{firstName: "test"},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {},
		courses: [],
		department: "Operations"
	}
)

/*
	[SECTION 4]: Delete Documents
		- directly deleting the document
	
		a. Delete Single Document
			Syntax:
				db.collectionName.deleteOne({criteria})
*/

	db.users.deleteOne({firstName: "Bill"})

/*
		b. Delete Multiple Document
*/
	db.users.deleteMany({firstName: "Jane"})

// REMEMBER: Not Recomended. All documents will be deleted
	db.users.deleteMany({})