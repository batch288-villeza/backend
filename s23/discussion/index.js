// console.log("Hello");

/*
	[SECTION 1]: JavaScript Objects
		- Object as a data type that is used to represent real world objects.
		- it is a collection a related data and/or functionalities
		- Objects uses property/key and value pair
		- we place another object, array or even function as the value for the object

		Object Structure:
			- key - is what we call the property of an object
			- value - is the value of the specific key or property.

		Creating object initializers/ object literal notation.

		Syntax:
			let objectName = {
				keyA: valueA,
				keyB: valueB,
				...
			}
*/
	let cellphone = {
		//key-value pairs
		name: 'Nokia 3210',
		manufacturerData: 1999
	}

	console.log('Result from creating objects using initializers/literal notaion:');
	console.log(cellphone);
	console.log(typeof cellphone);

/*
		a. Creating objects using constructor function
			- creates a reussable function to create several objects that have the same data structure.
			- this is useful for creating multiple instances/copies of an object.
			- an instance is a concrette occurance which emphasizes on the distinct/unique identity of it.

			Syntax:
				function objectName(valueA, vlaueB){
					this.keyA = valueA;
					this.keyB = valueB;
				}

			NOTE: Make the first letter of the constrctor function with a capital letter to differentiate it from ohter functions/classes

			- the "this." keyword allows us to assign a new objects properites by associating them with values recieved from a constructor's function parameters.
*/

	function Laptop(name, manufacturerDate){
		this.name = name;
		this.manufacturerDate = manufacturerDate;
	}

/*
	This is a unique instance of the Laptop object
		- the "new" operator creates an instances of an object
		- Objects and Instances are often interchanged because:
			- objects literals:  let object = {}
			- instances: let object = new object()
		  are distinct/unique objects.
*/
	let laptop = new Laptop("Lenovo", 2008);
	console.log('Result from creating objects using object constructor:');
	console.log(laptop);

	let myLaptop = new Laptop("Macbook Air", 2020);
	console.log('Result from creating objects using object constructor:');
	console.log(myLaptop);

	let oldLaptop = Laptop("Protal R2E CCMC", 1980);
	// the above example invokes/calls the "Laptop" function instead of creating a new objects instace
	// returns "undefined" without the "new" operator because the "Laptop" function does not have areturn statement
	console.log('Result from creating objects using object constructor:');
	console.log(oldLaptop);	

	// Create an empty objets
	let comuter = {}
	let myComputer = new Object();

/*
	[SECTION 2]: Accessing Object Properties
		-
*/
	// a. using dot notation
		console.log("Result from dot notation: "+myLaptop.name);

	// b. using [] notation
		console.log("Result from [] notation: "+myLaptop['name']);

	// c. Accessing array in objects
	/*		- ACcessing array elements can also be done using square brackets
			- Accesing object properties using the [] notation and array indexes can cause confusion.
			- by using dot notaion, this easily helps us differenttiate accessing elements from arrays and properties from objects.
			- Object properties have names that make it easier to associate pieces of information*/
		
		let array = [laptop, myLaptop]

	//		1. [] notation
		console.log("Result of accesing array using [] notation: "+array[0]['name']);
	// 		2. dot notaion
		console.log("Result accesing array using dot notation: "+array[0].name);

/*
	[SECTION 3]: Initializind/Adding, Deleting, Reassigning Objects Properties
		- Like other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared.
		- This is useful for times when object's properties are undetermined at the time of creating them
*/

		let car = {};

		// a. Initializing/adding object properties using:
		// 		1. dot notation
		car.name = 'Honda Civic';
		console.log("Result adding prperties using dotnotaion:");
		console.log(car);

		// 		2. dot notation
		/*			- while using the [] notation, it will allow access to spaces when assigning properly names to make it easier to read, this also makes it so that object properties can only be accessed using the [] notaion 
					- this also makes names of object properties to not follow commonly used naming conventions for them*/

		car['manufacture date'] = 2019;
		console.log(car['manufacture date']); // 2019
		console.log(car['manufacture Date']); // undefined
		console.log(car['Manufacture Date']); // undefined
		console.log(car.manufactureDate);	  // undefined
		console.log("Result adding prperties using [] ]notaion:");
		console.log(car); // {sample: 'Honda Civic', manufacture date: 2019}

		// b. Deleting Object Properties
		delete car['manufacture date'];
		console.log("Result of deleting prperties using [] notaion:");
		console.log(car); // {sample: 'Honda Civic'}

		// c. Reassigning Objects Properties
		car.name = 'Dodge Charger D/T'
		console.log("Result of reassigning object prperty:");
		console.log(car);

/*
	[SECTION 4]: Object Methods
		- a method is a function which is a property of an object
		- they are also function and one of the key differnces they have is that methods are functions related to a specific
		- methods are useful for creating object specific functions wich are sued to perform tasks on them
		- similar to functions/features of real world object, methods are defined based on what an objects is capable of doing and how it should work
*/

		let person = {
			name: 'John',
			talk: function talk(){
				console.log('Hello my name is ' + this.name);
				// return('Hello my name is ' + this.name);
			}
		}
		console.log(person)
		console.log("Result of reassigning object prperty:");
		person.talk();

		// adding methods to objects
		person.walk = function(){
			console.log(this.name + ' walked 25 steps forwad');
			// NOTE: pwede mo ilagay ito sa loob nung object person an same result parin when walk() is called.
		}
		person.walk();

		// Methods are useful for creating reusable functions that perform tasks relataed to objects
		let friend = {
			firstName: 'Joe',
			lastName: 'Smith',
			address: {
				city: 'Autin',
				state: 'Texas'
			},
			email: ['joe@mail.com', 'joesmith@mail.xyz'],
			introduce: function(){
				console.log(`Hello my name is ${this.firstName} ${this.lastName}`);
			}
		}
		console.log("Result of object:");
		console.log(friend);

		console.log("Result of calling object properties:");
		console.log(friend.firstName);
		console.log(friend.lastName);
		console.log(friend.address);
		console.log(friend.email);
		friend.introduce();

/*
	[SECTION 6]: Real World Application of Objects

	Scenarion:
		1. We would like to create a game that would have several pokemon interact with each other.
		2. Every pokemon would have the same set of stats, properties and functions.
*/
	// Using Object Literals to create multiple of pokemon would be time consuming
	let myPokemon = {
		name: "Pikachu",
		level: 3,
		health: 100,
		attack: 50,
		tackle: function(){
			console.log("This pokemon tackled targetPokemon");
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		},
		faint: function(){
			console.log("Pokemon fainted");
		}
	}
	console.log(myPokemon);

	// Using Object Constructor Function
	function Pokemon(name, level) {
		// Properties
		this.name = name;
		this.level = level;
		this.health = 2 * level;
		this.attack = level

		// Methods
		this.tackle = function(target){
			console.log(`${this.name} tackled ${target.name}`);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
		};
		this.faint = function(){
			console.log(this.name + ' fainted');
		};
	}
	// create new instances of "Pokemon" object each with their unique properties
	let pikachu = new Pokemon("Pikachu", 16);
	let rattata = new Pokemon("Rattata", 8);

	//  Providing the "rattata" object as an argument to the "pikachu" tackled method will create interaction between the two objects
	pikachu.tackle(rattata);
