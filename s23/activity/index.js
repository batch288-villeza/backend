// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
	// Initialize/add the given object properties and methods
	
	// Properties
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {hoenn: ["May", "Max"], kanto: ["Brock","Misty"]},
}

// Methods
trainer.talk = function(choosePokemon){
	console.log(`${this.pokemon[choosePokemon]}! I choose you!`)
}


// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
console.log("Result of dot notation:")
console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.friends);
console.log(trainer.pokemon);

// Access object properties using square bracket notation
console.log("Result of square bracket notation:")
console.log(trainer['name']);
console.log(trainer['age']);
console.log(trainer['friends']);
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log("Result of talk method:")
trainer.talk(0);

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;
}

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);
	pikachu.tackle = function(targetPokemon){
		let attackedPokemon = targetPokemon.name;
		let hp = targetPokemon.health - this.attack;
			targetPokemon.health = hp;

		console.log(`${this.name} tackled ${attackedPokemon}`);
		console.log(`${attackedPokemon}'s health is now reduced to ${hp}`);
		if(hp <= 0){
			this.faint(attackedPokemon);
		}
	}
	pikachu.faint = function(targetPokemon){
		console.log(`${targetPokemon} has fainted.`)
	}

console.log(pikachu);


// Create/instantiate a new pokemon
let geodude = new Pokemon("Geodude", 8);
	geodude.tackle = function(targetPokemon){
		let attackedPokemon = targetPokemon.name;
		let hp = targetPokemon.health - this.attack;
			targetPokemon.health = hp;
		console.log(`${this.name} tackled ${attackedPokemon}`);
		console.log(`${attackedPokemon}'s health is now reduced to ${hp}`);
		if(hp <= 0){
			this.faint(attackedPokemon);
		}
	}
	geodude.faint = function(targetPokemon){
		console.log(`${targetPokemon} has fainted.`)
	}

console.log(geodude);


// Create/instantiate a new pokemon
let mewtwo = new Pokemon("Mewtwo", 100);
	mewtwo.tackle = function(targetPokemon){
		let attackedPokemon = targetPokemon.name;
		let hp = targetPokemon.health - this.attack;
			targetPokemon.health = hp;
		console.log(`${this.name} tackled ${attackedPokemon}`);
		console.log(`${attackedPokemon}'s health is now reduced to ${hp}`);
		if(hp <= 0){
			this.faint(attackedPokemon);
		}
	}
	mewtwo.faint = function(targetPokemon){
		console.log(`${targetPokemon} has fainted.`)
	}

console.log(mewtwo);


// Invoke the tackle method and target a different object
geodude.tackle(pikachu);
console.log(pikachu);


// Invoke the tackle method and target a different object
mewtwo.tackle(geodude);
console.log(geodude);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}