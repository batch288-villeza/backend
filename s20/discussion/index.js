// console.log("Hello")

/*
	[SECTION 1]: While Loop
		- A while loop takes an expression/condition.
		- Expressions are any unit code that can be evaluated as true or false. If the condition evaluates to be a true, the statements/code block will be executed.
		- A loop will iterate a certain number of times until an expression/condition is true.
		- Iteration is the term given to repitition of statements.

		Syntax:
			while(expression/condition){
				statement;
				increment/decrement;
			}

		- check muna kung true yung condition bago mag run if false stop na.
*/

	// Example 1:
		let count = 5;

		while(count !== 0){

			// The urrent value of count is printed out.
			console.log("While: "+ count);

			// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
			/* IMPORTANT:	Forgetting to include this in our loops will make our applications run an infinite loop which will eventually crash our device
							After running the script, if a slow response from the browser is experienced or an infinite loop is seen in the console quickly close the application/browser/tab to avoid this.*/
			count--;
		}

/*
	[SECTION 2]: Do-While Loop
		- A do-while loop works a lot like while loop. But unlike while loop, do-while loops guarantees that the code will be executed at least once.

		Syntax:
			do{
				statement;
				increment/decrement;
			}while(condition/expression);

		- mag iterate muna nang 1x then saka i-check kung true or false yung condition. if true, it will proceed but if false mag stop ng yung iteration.
*/

	// Example 1: Comment the below code to check the do-while loop
		// let number = Number(prompt("Give me a number:")); // the Number() function converts data types into a number data type.
		// do{
		// 	console.log("Do-While: "+ number);
		// 	number++;
		// }while(number < 10);

/*
	[SECTION 3]: For Loop
		- a for loop is more flexible that while and do-while loops.
		- It consists of three parts:
			1. Initialization - value taht will track the progression of the loop.
			2. Expression/Condition - that will be evaluated which will determin whether the loop will run one more time.
			3. Iteration - indicates how to advance the loop.

		Syntax:
			for(initialization; condition/expression; iteration){
				statement;
			}

		- will create a loop that will start from 0 and will end at 20
		- Every iteration of the loop, the value of count will be checked if it is equal or less than 20.
		- if the value of count is less than or equal 20 the statement inside the lopp will execute
		- the value of count will be incremented by one for each iteration
*/

	// Example 1:
		for(count = 0; count <= 5; count++){
			console.log("The current value of count is: " + count);
		}

		// NOTE: you can use the Reassignment Operation "+=" in the iteration
		// for(count = 0; count <= 20; count+=2)

/*
	[SECTION 4]: Strings
*/
	// Example 1: Count the number characters in a string unsing .length Property
		
		let myString = "alex";
		console.log(`Total length of the string "${myString}" is: ${myString.length}`);

	// Example 2: Access elements of a string to the characters of the string ising indexing.
		console.log(myString[0]); //Result to "a"
		console.log(myString[3]); //Result to "x"
		console.log(myString[5]); //Result to "undefined"

	// Example 3: We will create loop that will print out the individual letters of the myString
		for(let index = 0; index < myString.length; index++){
			console.log(myString[index]);
		}

	// Example 4: Create a string named "myName" with value of "Alex" and replace all vowels by 3
		let myName = "Alex";
		for(let index=0; index < myName.length; index++){

			// console.log(myName[index]);

			if (myName[index].toLowerCase() === "a" ||
				myName[index].toLowerCase() === "e" ||
				myName[index].toLowerCase() === "i" ||
				myName[index].toLowerCase() === "o" ||
				myName[index].toLowerCase() === "u"){
				
				console.log(3)

			}else{
				console.log(myName[index])
			}
		}

	// Example 5: Replace Property
		let myNikcName = "Chis Mortel";
		let upadatedNickName = myNikcName.replace("Chis", "Chris");
		console.log(myNikcName);
		console.log(upadatedNickName);


/*
	[SECTION 5]: Continue or Break Statements
		a. Continue Statement
			- let the code proceed to the next iteration of the loop without finishing the execution of all statements in the code block.
			- bale yung continue, it will stop the current iteration, and start another iteration or proceed to another iteration
		b. Break Statements
			- is used to terminate the current loop once a match has been found.
*/
	// Example 1: Create a loop that if the count value is divided by and the remain is 0, it will log the number and continue to the next iteration of the loop.
		for(let count = 0; count <= 20; count++){
			if(count >= 10){
				console.log("The number is equal to 10, stopping the loop");
				break;
			}

			if(count % 2 === 0){
				console.log("The number is equal to two, skipping . . .");
				continue;
			}
			console.log(count);
		}

	// Example 2: 	- Create a loop that will iterate based on the length of the string name.
	// 				- We are going to console the letters per line, and if the letter is equal to "a" we are going to console "Continue to the next iteration" then add continue statement
	// 				- If the current letter is equal to "d" we are going to stop the loop.
		let name = "alexandro";
		for(let index=0; index<name.length; index++){
			if(name[index] === "a"){
				console.log("Continue to the next iteration");
				continue;
			}else if(name[index] === "d"){
				console.log("Iteration stops!");
				break;
			}else{
				console.log(name[index])
			}
		}