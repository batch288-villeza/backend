// console.log("Hello");

/*
	[SECTION 1]: Array Manipulation
		I. Array Methods
			- JavaScript has built-in functions and method for arrays.
			- This allows us to manipuplate and access array items.

			a. Mutator Method
				- this are functions that "mutate" or change an array after they are created.
				- these methods manipulated the original array performs suach as adding or removing elements.
*/
	// Declare Array Variable
	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
/*
				1.  push() Method
					- adds an element at the end of an array an returns the updated array's length

					Syntax:
						- arrayName.push();

*/
	//	Example 1:
	console.log("Current Array:");
	console.log(fruits);
	
	fruits.push('Manggo');

	console.log("Mutated array after push() method:");
	console.log(fruits);
	console.log(fruits.length);

	// Example 2: Push Multiple elements
	fruits.push('Avocado', 'Guava');
	console.log("Mutated array after pushing multiple elements");
	console.log(fruits);
	console.log(fruits.length);

/*
				2. pop() Method
					- removes the last element and returns the remove element.

					Syntax:
						- arrayName.pop();
*/
	console.log("Current Array:");
	console.log(fruits);
	
	// fruits.pop();
	let removedFruit = fruits.pop();
	console.log(removedFruit);

	console.log("Mutated array after pop() method:");
	console.log(fruits);
	console.log(fruits.length);

/*
			3. unshift() Method
				- adds one or more elements at the beginning of an array.
				- it returns the updated array length

				Syntax:
					- arrayName.unshift('elementA', 'elementb', ...);
*/
	console.log("Current Array:");
	console.log(fruits);

	fruits.unshift('Lime', 'Banana');
	console.log("Mutated array after unshift() method:");
	console.log(fruits);
	console.log(fruits.length);

/*
			4. shift() Method
				- remove element at the beginning of the array and returns the removed element.

				Syntax:
					- arrayName.shift();
*/
	console.log("Current Array:");
	console.log(fruits);

	fruits.shift();
	removedFruit = fruits.shift();
		console.log(removedFruit);
	console.log("Mutated array after shift() method:");
	console.log(fruits);
	console.log(fruits.length);



/*
			5. splice() Method
				- simultaneously removes a elements from a specified index and adds element
				Syntax:
					- arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
	console.log("Current Array:");
	console.log(fruits);

	fruits.splice(3); // select 3 element
	// fruits.splice(3 ,1); // remove 1 element starting from index 3
	// fruits.splice(1, 3, 'Lime', 'Cherry'); // remove 3 elements starting from index 1, and replace the elements removed.
	// fruits.splice(1, 0, 'Grapes', 'Strawberry'); // add elements before index 1
	// fruits.splice(0, 0, 'Grapes', 'Strawberry'); // add elements at the begining of the array
	// fruits.splice(fruits.length-1, 1,); // remove the last element from the list
	// fruits.splice(fruits.length, 0, 'Cherry'); // add at the end of the array
	console.log("Mutated array after splice() method:");
	console.log(fruits);
	console.log(fruits.length);

/*
			6. sort() Method
				- rearrange the array elements in alphanumeric order
				Syntax:
					- arrayName.sort();
*/
	console.log("Current Array:");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array after sort() method:");
	console.log(fruits);

/*
			6. reverse() Method
				- reverses the order of the array elements
				Syntax:
					- arrayName.reverse();
*/
	console.log("Current Array:");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array after sort() method:");
	console.log(fruits);

/*
		b. Non-Mutator Methods
			- functions that do not modify or change an array after they are created.
			- these methods do not manipulate the original array, performing various tasks such as returning elements from an array and combining arrays and printing the output.
*/

			let countries =['US','PH','CAN','SG','TH','PH','FR','DE'];
/*
			1. 	indexOf() Methods
				- returns the index number of the first matching elements found in an array.
				- if no match was found, the result will be -1.
				- the search processs will be done from the first element proceeding to the last element

				Syntax:
					- arrayName.indexOf(searchValue);
					- arrayName.indexOf(searchValue, startingIndex);
				NOTE: sa unahan ng array mag start ang search
*/
			let firstIndex = countries.indexOf('PH')
			console.log(firstIndex);

			let invalidCountry = countries.indexOf('BR')
			console.log(invalidCountry);

			// Starting index
			firstIndex = countries.indexOf('PH', 2)
			console.log(firstIndex);

			console.log(countries)

/*
			1. 	lastIndexOf() Methods
				- returns the index number of the last matching elements found in an array.
				- if no match was found, the result will be -1.
				- the search processs will be done from the last element proceeding to the first element.


				Syntax:
					- arrayName.lastIndexOf(searchValue);
					- arrayName.lastIndexOf(searchValue, startingIndex);

				NOTE: sa dulo nang array mag start yung search
*/
			let lastIndex = countries.lastIndexOf('PH');
			console.log(lastIndex);

			invalidCountry = countries.lastIndexOf('BR')
			console.log(invalidCountry);

			// Starting index
			lastIndex = countries.lastIndexOf('PH', 4)
			console.log(lastIndex);

/*
			2. slice() Method
				- portions/slices elements from an array and retun a new array
				- slicing off elements from a specified index to the last element.
				- 
				Syntax:
					- arrayName.slice(startingIndex);
					- arrayName.slice(startingIndex, endingIndex);
*/
			// startIndex
			let slicedArrayA = countries.slice(2);
			console.log("Result from sliced() Method:");
			console.log(slicedArrayA);

			// startingIndex, endingIndex
			let slicedArrayB = countries.slice(2, 4);
			console.log("Result from sliced() Method:");
			console.log(slicedArrayB);

			// slicing elements using negative number
			let slicedArrayC = countries.slice(-3);
			console.log("Result from sliced() Method:");
			console.log(slicedArrayC);


/*
			3. toString() Method
				- return array as string separateed by commas
				Syntax:
					- arrayName.toString();
*/
			let stringArray = countries.toString();
			console.log("Result from sliced() Method:");
			console.log(stringArray);
			console.log(typeof stringArray);

/*
			4. concat() Method
				- combines arrays into 1 array
				Syntax:
					- arrayA.concat(arrayB);
					- arrayA.concat(elementA);
*/
			let tasksArrayA = ["drink HTML", "eat JavaScript"];
			let tasksArrayB = ["inhale CSS", "breathe sass"];
			let tasksArrayC = ["get git", "be node"];

			let tasks = tasksArrayA.concat(tasksArrayB);
			console.log(tasks);

			let combinedTasks = tasksArrayA.concat('smell express', 'throw react')
			console.log(combinedTasks)

			let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC)
			console.log(allTasks);

			// concat array to an array and element
			let exampleTasks = tasksArrayA.concat(tasksArrayB, 'smell express');
			console.log(exampleTasks);

/*
			5. join() Method
				- returns array as a string seperated by specified separator
				
				Syntax:
					- arrayName.join("separator")
*/
			let users = ['John', 'Jane', 'Joe', 'Jacob'];

			console.log(users.join());
			console.log(users.join(''));
			console.log(users.join('-'));

/*
	[SECTION 2]: Array Iteration Method
		- Iteration methods are loops designed to perform repititive tasks
		- iteration methods loops over all items in an array

		a. forEach()
			- similar to a for loop that iterates on each of array element.
			Syntax:
				arrayname.forEach(function(individualElement{statement};));
*/		
		// Simple forEach()
			console.log(allTasks);
			allTasks.forEach(
				function(task){
					console.log('This line '+task);
				}
			);

		// filteredTasks variable will hold all the elements from the allTaks array that has more than 10 characters
			
			let filteredTasks = []

			allTasks.forEach(
					function(tasks) {
						if(tasks.length > 10){
							filteredTasks.push(tasks);
						}
					}
				);
			console.log(filteredTasks);
/*
		b. map()
			- Iterates on each element and returns new array with different values depending on the result of the function's operation.
*/
			console.log('map()');
			let numbers = [1,2,3,4,5];

			let numberMap = numbers.map(
					function(number) {
						return number * number;
					}
				);
			console.log(numbers);
			console.log(numberMap);

/*
		b. every()
			- it will check all elements in an array meet the given condition
			- return true value if all elements meet the condition and false otherwise

			Syntax:
				- let/const resultArray = arrayName.every(function(indivElemenet){return expresssion/condition};)

			NOTE: Dapat lahat nang result sa condition ay true para mag true kapag may isang false, false na ang return
*/
		numbers = [1,2,3,4,5];
		let allValid = numbers.every(function(number){
					return (number<3)
				}
			);

/*
		c. some()
			- checks if at least one element in the array meets the given condition.
			- 
			Syntax:
				let/const result = arrayName.some(function(indivElement){return expression/condition;})

			NOTE: parang "or" operator basta may true, mag rereturn ng true
*/
		numbers = [1,2,3,4,5];
		let someValid = numbers.every(function(number){
					return (number<2);
				}
			);
		console.log(someValid);

/*
		d. filter()
			- returns new array that conatins elements which meets the given
			- returns an empty array if no lement were found
			Syntax:
				- let/const resultArray = arrayName.filter(function(indivElement){return expression/conditon;})

			NOTE: Lahat lang nang nasa array na nag true sa condition yun ang mag reretrun
*/
		numbers = [1,2,3,4,5];
		let filterValid = numbers.filter(function(number){
					return (number % 2 === 0);
				}
			);
		console.log(filterValid);

/*
		e. includes()
			- checks if the argument passed can be found in the array
			- this is case sensitive

		NOTE: return true kung existing yung sa array yung nasa includes()
*/
		let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
		let productFound1 = products.includes('Mouse');
		console.log(productFound1);

/*
		f. reduce() Method
			- evaluates elements from left to right and returns/reduces the array in a single value.
			- the first parameter in the function will be accumulator
			- the 2nd parameter in the function will be the currentValue
			Syntax:
				- let/const resultArray = arrayName.reduce(function(accumulator, currentValue){return expression;})

			NOTE: Pinagsasama lang nya lahat ng nasa array

*/
		// Example 1: number
		numbers = [1,2,3,4,5];
		let reduceArray = numbers.reduce(function(x, y){
					// console.log('Accumulator '+x);
					// console.log('currentValue '+y);
					return x+y;
				}
			);
		console.log(reduceArray);

		// Example 2: string
		products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
		let reduceArrayString = products.reduce(function(x, y){
					return x+y;
				}
			);
		console.log(reduceArrayString);