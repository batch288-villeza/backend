// console.log("Hello");

/*
	[SECTION I]: FUNCTION
		- Functionns in JavaScript are lines/block of codes that tell our device/application to perform a certian tasks when called/invoked.
		- Mostly created to create complicated tasks to run several lines of codes in succession.

		1. Function Declarations
			- defines a function with the specified paramaeters

			SYNTAX:
				function functionaName(){
					codeBlock/statements;
				};

			a. function keyword - used to define a JavaScript Function
			b. funtionName - is named to be able to use later in code for invocation
			c. function block ({}) - this is where the code will be executed
*/
			// Example:
			function printName(){
				console.log("My name is John");
			};

/*
		2. Function Invocation
			- the code block and statements inside a function is not immediately executed when the function is defined.
			- the code block and statements inside a function is executed when the function is invoked or called.
			- It is common to use the term "call a function" instead of "invoke a function".
*/
			// Let's now call the "printName()" we declared
			printName();

/*
		3. Function Declaration vs. Expression

			a. Function Declaration
				- A function can be created through function declaration by busing the using the "function" keyword and adding the function name
				- Declared function/s can be hoisted.
*/
				// Example:

				declaredFunction();
				// Note: Hoisting is a JavaScript behavior for certain variables and functions to run or use before their declaration

				function declaredFunction(){
					console.log("Hello World from declaredFunction!");
				};
/*
			b. Function expression
				- a function can be also be stored in a variable.
				- a function expression is an anonymous function assigned to a variable function.
*/
				// variableFunction(); // remve comment to see if you can call Function Expression first before assigning it.

				// Anonymous Function Example:
				/*function(){
					console.log("anonymous function");
				};*/

				// Anonymous Function as Vairable Example:
				let variableFunction = function(){
					console.log("anonymous function");
				};

				variableFunction();

				// we can also create a function expression of named function
				let functionExpression = function funcName(){
					console.log("This is a Function Expression of a Named Function");
				};
				functionExpression();


				// You can re-assign declared function and function expressions to new anonymous function.
				declaredFunction = function(){
					console.log("Updated declaredFunction!");
				}
				declaredFunction();

				functionExpression = function(){
					console.log("Updated functionExpression");
				};
				functionExpression();

				// However, we cannot reassinged a function expression initialized with "const" keyword.
				const contantFunc = function(){
					console.log("Initialized with contant variable!");
				};
				contantFunc();

				// Reassigned constant functions
			/*	contantFunc = function(){
					console.log("Cannot be re-assigned!");
				};
				contantFunc();
			*/

/*
	[SECTION II]: FUNCTIONS SCOPE and NESTED FUNCTION
		-
*/

// Function Scoping
		// Scope it is the accessiblity of variables within our program.

		/*
			JavaScript Variables has 3 types of scope:
			1. Global
			2. local/block scope
			4. function scope
		*/

		{
			let localVar = "Armando Peres";


		}

		//Result in error. localVar, being in a block, it will not be accessible outside the block.
		/*console.log(localVar);*/


		let globalVar = "Mr. Worldwide";

		{
			console.log(globalVar);
		}

	//Function Scope
		// JavaScript has function scope: Each function creates a new scope.
		// Variables defined inside a function are not accessible (visible) from outside the function.

		function showNames(){
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionConst);
			console.log(functionLet);
		}

		/*console.log(functionConst);*/
		// console.log(functionLet);

		showNames();

	// Nested functions
		//You can create anothe function inside a function. This is called a nested function.

		// function myNewFunction(){
		// 	let name = "Jane";

		// 	// nested function
		// 	function nestedFunction(){
		// 		console.log(name);
		// 	}

		// 	nestedFunction();
		// }

		// myNewFunction();

		// let functionExpression = function(){

		// 	function nestedFunction(){
		// 		let greetings = "Hello Batch 288!";
		// 		console.log(greetings);
		// 	}

		// 	nestedFunction();
		// }

		// functionExpression();




/*
	[SECTION III]: RETURN STATEMENT
		- The "RETURN STATEMENT" allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.
*/

	// Example:
	function returnFullName(){
		let fullName = "Jeffrey Smith Bezos";
		// let returnArray = [1,2,3,4,5]
		
		return fullName;
		// return returnArray;

		// console.log("Hello, after the return keyword!")
	};

	// console.log(returnFullName());


	let fullName = returnFullName();
	console.log(fullName);


/*
	[SECTION IV]: Function Naming Conventions
		a. Function names should be definitive of the task it will perform. It usually contains a verb.
*/

	function getCourse(){
		let course = ["Science 101", "Math 101", "English 101"];

		return course;
	};
	console.log(getCourse());

	// b.

	

	// c. Avoid pointless and inappropriate function names

	function foo(){
		return 25%5;
	};

	// 

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
	};