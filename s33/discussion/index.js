/*	Section I: JavaScript Synchronous vs Asynchronous
		- JavaScript is synchronous by default, meaning only one statement is executed at a time.
		- This can be proveb when a statement has an error, javascript will not proceed with the next statement/*/

/*	Example:

	console.log("Hello World!");

	conole.log("Hello");

	console.log("Hello");
*/

// When certain statements take a lot of time to process, this slows down the our code.
	// for(let index=0; index<=1500; index++){
	// 	console.log(index);
	// }
	// console.log("Hello again!");


/*	b. Asyncronous
		- means that  we can proceed to execute other statements, while time consuming code is running in the background.*/

/*	Section II: Getting all posts
		> Fetch API - allows us to asynchronously request for resource data.
					- so it means the Fetch Method that we are going to use heare will run asynchronously.

		Syntax:
			fetch('URL');
*/
//		> "promise" - is an object taht represents the eventual completion (or failure) of an asynchronous function and it's result value.
			// console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*		a. fetch() and .then()

			Syntax:
				fetch('URl').then(response => (response));
*/	
	
// 			> Retrieve all posts follow the REST API
// 			> The fetch method will return a promise that resolves the response object
// 			> the "then" method captures the response object
				// fetch('https://jsonplaceholder.typicode.com/posts')
				// .then(response => console.log(response));
	
// 			> use the ".json" method from the response object to convert the data retrievd into JSON format to be used in our application
				// fetch('https://jsonplaceholder.typicode.com/posts')
				// .then(response => {
				// 	console.log(response.json())
				// });
	
// 			> 
				// fetch('https://jsonplaceholder.typicode.com/posts')
				// .then(response => {
				// 	return response.json()
				// });
	
// 			> simpified verion
				// fetch('https://jsonplaceholder.typicode.com/posts')
				// 	.then(response => response.json())
				// 	.then(json => console.log(json))
	
/*			NOTE:
				1. function has an implicit return.
				2. the "response" and "json" inside the .then() method are parameters of a function that is a user defined, it means that it can be changed.
*/

/* 		b. "async" and "await" keyword 
 			> "async" and "await" keyword
 				- it is another approach that can be used to achieve asynchronous code
 			> "async"
				-
 			> "await"
 				-
*/

// 			Creates an asynchrnous function:		
				// async function fetchData(){
				// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
				// 	console.log(result);
				
				// 	let json = await result.json();
				// 	console.log(json);
				// };

				// fetchData();


/*	Section III: Getting Specific Post (GET)
		> Retrieve specific post following the REST API(/posts/:id)
*/

		// fetch('https://jsonplaceholder.typicode.com/posts/5') // returns a "promise"
			// .then(response => console.log(response)) // return a reponse object
			// .then(response => response.json()) // converts response into a json file
			// .then(json => console.log(json)) // display the json object

/*	Section IV: Creating Post (POST)
		> option - it is an object that contains the method, the header and the body of the request

		Syntax:
			fetch('URL', {option})
				.then(response => {})
				.then(response => {})
				...
*/
		fetch('https://jsonplaceholder.typicode.com/posts/', {
			//sets the method of the reuqest object to post the following REST API.
			method: 'POST',
			
			// sets the header data of the request objectto be sent to the backend.
			// specified that the content will be in JSON structure
			headers: {
				'Content-Type': 'application/json'
			},

			//sets the content/body data of the request object to be sent backend
			body: JSON.stringify(
					{
						title: 'New post',
						body: 'Hello world',
						userId: 1
					}
				)
		})
		.then(response => response.json())
		.then(json => console.log(json));


/* SECTION V: Update a specific post (PUT)
	
*/
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(
					{
						id: 1,
						title: 'Updated Post',
						body: 'Hello again!',
						userId: 1
					}
				)
		})
		.then(response => response.json())
		.then(json => console.log(json));


		
		// PUT - a method of modifying resource where the client send data that updates the entire object/document
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(
					{
						title: 'Updated Post'
					}
				)
		})
		.then(response => response.json())
		.then(json => console.log(json));

		// PATCH - applies a partial update to the object or document
		fetch('https://jsonplaceholder.typicode.com/posts/1',{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(
					{
						title: 'Updated Post'
					}
				)
		})
		.then(response => response.json())
		.then(json => console.log(json));


/*	Section VI: Deleting a post (DELETE)
*/
	// deleteing specific post following the REST API
	fetch('https://jsonplaceholder.typicode.com/posts/1', {method: 'DELETE'})
		.then(response => response.json())
		.then(json => console.log(json));
